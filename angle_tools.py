########################################
# PYGAME ANGLES:   VECTOR2 ANGLES:     #
#                                      #
#      0                 270           #
#   90   270          180    0         #
#     180                 90           #
########################################


def angle_difference(angle1, angle2):
    """ Return the difference of two angles (in degrees)  """
    while angle1 >= 360:
        angle1 -= 360
    while angle1 <= -360:
        angle1 += 360
    while angle2 >= 360:
        angle2 -= 360
    while angle2 <= -360:
        angle2 += 360
    diff = angle1 - angle2
    if diff > 180:
        diff -= 360
    if diff < -180:
        diff += 360
    return diff
