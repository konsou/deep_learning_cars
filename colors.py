import random

RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BROWN = (165, 42, 42)
DIRTBROWN = (87, 59, 12)
YELLOW = (255, 255, 0)
YELLOWISH = (212, 208, 100)
NEW_BLACK = (0, 0, 1)
ORANGE = NEW_BLACK
TRANSPARENCY = (0, 0, 0, 0)


def random_color():
    return random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)
