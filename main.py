import sys
import os
import multiprocessing
from level import Level
from car import RaceCar
from sensor import PointSensorTemplate, LineSensorTemplate, PointSensor, LineSensor
from sensor import SpeedSensorTemplate
from utility import intify_tuple
from brains.brain_base import BaseBrain
from brains.brain_konso_sigmoid import SigmoidBrain, ThreeLayerBrain, KonsoThreeLayerTagBrain, KonsoThreeLayerRaceBrain
from brains.brain_konso_basic import BasicBrain
from brain_archive import BrainArchive, load_archive
from text import Text
from ui import InfoBlurb
from colors import *
from groups import *
from constants import *
from pygame.locals import *

AVAILABLE_BRAINS = [KonsoThreeLayerRaceBrain]
# AVAILABLE_BRAINS = [SigmoidBrain]


class Program(multiprocessing.Process):
    def __init__(self, is_subprocess=False,
                 command_queue: multiprocessing.JoinableQueue = None,
                 result_queue: multiprocessing.Queue = None):
        multiprocessing.Process.__init__(self)
        self.is_subprocess = is_subprocess
        pid = os.getpid()

        if is_subprocess:
            self.command_queue = command_queue
            self.result_queue = result_queue

        pygame.init()
        self.win = pygame.display.set_mode((800, 600))
        pygame.display.set_caption(f"Deep learning cars - {pid}")

        print(f"Process ID is {pid}")
        self.clock = pygame.time.Clock()

        # Fast forward mode
        self.fast_forward = False
        # self.draw_graphics = True
        self.paused = False
        self.slo_mo = False
        self.best_car_last_known_pos = -100, -100
        self.highest_points_this_round = 0
        self.best_car_this_round = None

        # UI
        self.mouse_hover_info_blurb = InfoBlurb(info_data={"ID": 0, "Score": 0})

        # Sensor templates
        # First sensor is always car speed - not included here
        self.sensor_templates = [
            SpeedSensorTemplate(label="Speed"),
            LineSensorTemplate(label="Wall left",
                               angle=45, distance=LINEAR_SENSOR_RANGE,
                               sensor_resolution=SENSOR_RESOLUTION,
                               show_sensors=SHOW_SENSORS),
            LineSensorTemplate(label="Wall center",
                               angle=0, distance=LINEAR_SENSOR_RANGE,
                               sensor_resolution=SENSOR_RESOLUTION,
                               show_sensors=SHOW_SENSORS),
            LineSensorTemplate(label="Wall right",
                               angle=-45, distance=LINEAR_SENSOR_RANGE,
                               sensor_resolution=SENSOR_RESOLUTION,
                               show_sensors=SHOW_SENSORS),

            # LineSensorTemplate(angle=0),
            # LineSensorTemplate(angle=20),
            # LineSensorTemplate(angle=-20),
            # LineSensorTemplate(angle=45),
            # LineSensorTemplate(angle=-45),
            # LineSensorTemplate(angle=80),
            # LineSensorTemplate(angle=-80),
        ]

        # Calculate number of sensor values
        # sensor_outputs = 0
        # for sensor_template_ in self.sensor_templates:
        #     sensor_outputs += sensor_template_.number_of_outputs

        # Currently hardocded at 8 :/
        self.number_of_sensors = 8

        # Levels
        self.available_levels = [
            # Level(name='Ugly Donut CW', image='levels/level1.png', start_pos=(350, 500), start_direction=90,
            #       laps=1, lap_minimum_distance=1200),
            # Level(name='Ugly Donut CCW', image='levels/level1.png', start_pos=(350, 500), start_direction=-90,
            #       laps=1, lap_minimum_distance=1200),
            # Level(name='Level 2 CCW', image='levels/level2.png', start_pos=(350, 555), start_direction=-90,
            #       laps=1, lap_minimum_distance=1200),
            # Level(name='Level 2 CW', image='levels/level2.png', start_pos=(350, 555), start_direction=90,
            #       laps=1, lap_minimum_distance=1200),
            # Level(name='Level 3 CCW', image='levels/level3.png', start_pos=(370, 510), start_direction=-90,
            #       laps=1, lap_minimum_distance=1200),
            # Level(name='Level 3 CW', image='levels/level3.png', start_pos=(370, 510), start_direction=90,
            #       laps=1, lap_minimum_distance=1200),
            Level(name='Complex, width 50 CCW', image='levels/level4 - complex - width 50.png', start_pos=(400, 580),
                  start_direction=-90, laps=1, lap_minimum_distance=1200),
            Level(name='Complex, width 50 CW', image='levels/level4 - complex - width 50.png', start_pos=(400, 580),
                  start_direction=90, laps=1, lap_minimum_distance=1200),
            # Level(name='TursaLevel 2 CCW', image='levels/tursalevel 2.png', start_pos=(255, 485),
            #       start_direction=0, laps=2, lap_minimum_distance=1200),
            # Level(name='TursaLevel 2 CW', image='levels/tursalevel 2.png', start_pos=(255, 485),
            #       start_direction=180, laps=1, lap_minimum_distance=1200),
            ]

        self.level = None
        self._current_level_index = -1
        self.select_next_level()

        # Archives - load from disk if possible
        try:
            self.archive = load_archive()
            # Set BaseBrain id counter properly
            BaseBrain.id_counter = self.archive.highest_id_in_use + 1
            print(f"BaseBrain id counter set to {BaseBrain.id_counter}")
        except OSError:
            self.archive = BrainArchive()

        self.inactive_cars = []
        self.crashed_cars = []
        self.finished_cars = []

        self.create_cars_and_brains()

        if not self.is_subprocess:
            self.run()

    def select_next_level(self):
        print(f"Selecting next level...")
        self._select_level(self._current_level_index + 1)

    def _select_level(self, level_number: int):
        self._current_level_index = level_number

        if self.level is not None:
            self.level.finish_line.kill()
            self.level.kill()
        self.level = self.available_levels[self._current_level_index]
        self.level.add(LEVEL_GROUP)
        self.level.finish_line.add(FINISH_LINE_GROUP)
        # print(f"Selected level {self.level}")

    def create_cars_and_brains(self):
        print(f"---------------NEW ROUND---------------")
        print(f"Spawning brains")
        self.inactive_cars = []
        self.crashed_cars = []
        self.finished_cars = []
        self.highest_points_this_round = 0
        self.best_car_this_round = None
        # print(f"Current archive: {self.archive.archive}")

        # Pick cars from archive, clone them
        # if len(self.archive) >= BRAIN_ARCHIVE_PICKS_PER_ROUND:
        first_picks = []
        selected_brains = []

        # Select up to BRAIN_ARCHIVE_PICKS_PER_ROUND brains from the archive
        for brain_ in list(self.archive.archive)[:BRAIN_ARCHIVE_PICKS_PER_ROUND]:
            first_picks.append(brain_)

        # print(f"Archive brain picks: {first_picks}")

        # Clone the selection
        for brain in first_picks:
            selected_brains.append(brain)

            # print(f"Parent: {parent_brain}")
            for _ in range(CLONES_PER_ARCHIVE_PICK):
                selected_brains.append(brain.clone_and_evolve())
            # print(f"Clone:  {brain_copy}")

        # print(f"Brains cloned")

        # Not enough brains for all cars? (Archive too small) - generate new brains until we have enough
        while len(selected_brains) < NUMBER_OF_CARS:
            selected_brains.append(random.choice(AVAILABLE_BRAINS)(number_of_inputs=self.number_of_sensors))

        # print(f"Selected brains: {selected_brains}")

        # print(f"Creating cars")
        print(f"Level is {self.level.name}")

        # Inform Brains that a new round has started
        for brain_ in selected_brains:
            brain_.start_new_round()

        # Create Cars
        for i in range(NUMBER_OF_CARS):
            RaceCar(level=self.level, brain=selected_brains[i],
                    pos=self.level.start_pos, heading=self.level.start_direction,
                    active_groups=ACTIVE_CAR_GROUP,
                    inactive_group=INACTIVE_CAR_GROUP,
                    sensors_group=LINE_SENSORS_GROUP,
                    sensor_templates=self.sensor_templates,
                    brain_reward_values={},
                    fuel=CAR_FUEL,
                    max_speed=CAR_MAX_SPEED,
                    min_speed=CAR_MIN_SPEED,
                    turning_speed=CAR_TURNING_SPEED,
                    acceleration=CAR_ACCELERATION,
                    braking=CAR_BRAKING,
                    friction=0,
                    )

        # print(ACTIVE_CAR_GROUP)

    def run(self):
        self.running = True

        ##################
        # EVENT HANDLING #
        ##################

        while self.running:
            if self.is_subprocess:
                # Command queue handling here
                pass
            else:
                for event in pygame.event.get():
                    if event.type == QUIT:
                        self.running = False
                    elif event.type == KEYUP:
                        if event.key == K_ESCAPE:
                            self.running = False
                        elif event.key == K_SPACE:
                            # Space toggles fast forward
                            self.fast_forward = not self.fast_forward
                            print(f"Fast forward: {self.fast_forward}")
                        elif event.key == K_g:
                            # 'g' toggles raphics display
                            global DRAW_GRAPHICS
                            DRAW_GRAPHICS = not DRAW_GRAPHICS
                            print(f"DRAW GRAPHICS: {DRAW_GRAPHICS}")
                        elif event.key == K_s:
                            # 's' toggles sensor display
                            global SHOW_SENSORS
                            SHOW_SENSORS = not SHOW_SENSORS
                            print(f"SHOW SENSORS: {SHOW_SENSORS}")
                        elif event.key == K_p:
                            self.paused = not self.paused
                            print(f"PAUSED: {self.paused}")
                        elif event.key == K_RSHIFT or event.key == K_LSHIFT:
                            self.slo_mo = not self.slo_mo
                            print(f"SLO-MO: {self.slo_mo}")

            ###############
            # CAR UPDATES #
            ###############

            if not self.paused:
                ACTIVE_CAR_GROUP.update(self.level.rect, self.is_subprocess)

            ####################
            # COLLISION CHECKS #
            ####################

            # "Collisions" between mouse and car
            # check mouse hover over cars
            hover_car = None
            if DRAW_GRAPHICS:
                mouse_pos = pygame.mouse.get_pos()
                for car in ACTIVE_CAR_GROUP:
                    if car.rect.collidepoint(mouse_pos):
                        hover_car = car
                        car_pos = car.get_position_as_int()

                        self.mouse_hover_info_blurb.left = mouse_pos[0] + 20
                        self.mouse_hover_info_blurb.y = mouse_pos[1]

                        self.mouse_hover_info_blurb.info_data = {"ID": hover_car.brain.id,
                                                                 "Score": hover_car.brain.points_this_round}
                        break

            if not self.paused:
                # Collisions with walls
                collisions = pygame.sprite.groupcollide(ACTIVE_CAR_GROUP, LEVEL_GROUP, dokilla=False, dokillb=False,
                                                        collided=pygame.sprite.collide_mask)
                for car in collisions:
                    self.crashed_cars.append(car)
                    self.inactive_cars.append(car)
                    car.crash()
                # print(collisions)

                # CHECK LAP FINISHED
                lap_finished = pygame.sprite.groupcollide(ACTIVE_CAR_GROUP, FINISH_LINE_GROUP,
                                                          dokilla=False, dokillb=False,
                                                          collided=pygame.sprite.collide_mask)

                # print(f"Lap finished: {lap_finished}")
                for car in lap_finished:
                    car.check_lap_finished()
                    if car.laps_completed >= self.level.laps:
                        car.kill()
                        # car.label_text.kill()
                        # for sensor in car.sensors:
                        #     sensor.kill()
                        self.finished_cars.append(car)
                        self.inactive_cars.append(car)
                    else:
                        car.brain.laps_started += 1

                # No cars? Add the best non-duplicate brain from inactive cars to archive
                if len(ACTIVE_CAR_GROUP) <= 0:
                    brains = [car.brain for car in self.inactive_cars]
                    brains_sorted = BaseBrain.sort_by_attribute(brains, 'points_this_round')
                    best_brain_this_round = brains_sorted[0]

                    print(f"PID {self.pid} best brain for this round: {best_brain_this_round}")
                    # print(f"All brains (sorted): {brains_sorted}")

                    # Add the best non-duplicate brain to archive
                    for brain_ in brains_sorted:
                        # print(brain_)

                        if self.archive.add_brain_dont_remove_best(brain_):
                            break
                    # self.add_brains_to_archive(self.crashed_cars)

                    # Save archive after every round
                    self.archive.save()

                    # No finished cars - respawn in the same level if LOOP_LEVEL_UNTIL_CAR_FINISHES is set
                    if len(self.finished_cars) == 0 and LOOP_LEVEL_UNTIL_CAR_FINISHES:
                        # print(f"Crashed cars: {self.crashed_cars}")
                        # brains_list = BaseBrain.sort_by_attribute([car.brain for car in self.crashed_cars])
                        # print(f"All cars crashed.")  #  Sorted brains: {brains_list}")
                        self.create_cars_and_brains()
                    # There were finished cars OR LOOP_LEVEL_UNTIL_CAR_FINISHES not set - next level
                    else:
                        print(f"ROUND FINISHED")
                        # self.add_brains_to_archive(self.finished_cars)
                        for i, car in enumerate(self.finished_cars):
                            # Add points for brains that finished first
                            # print(car.brain.points)
                            # car.brain.add_points(POINTS_PER_FINISH_POSITION.get(i, 0))
                            # print(car.brain.points)
                            car.brain.level_finished(self.level.name)

                        # Next level
                        try:
                            self.select_next_level()
                            self.create_cars_and_brains()
                        except IndexError:
                            print(f"ALL LEVELS FINISHED")
                            if FOREVER_TRAINING_MODE:
                                self._select_level(0)
                                self.create_cars_and_brains()
                            else:
                                self.running = False

            ##########################
            # DRAWING TO SCREEN HERE #
            ##########################

            if not self.is_subprocess and DRAW_GRAPHICS:
                self.win.fill(WHITE)

                LEVEL_GROUP.draw(self.win)
                FINISH_LINE_GROUP.draw(self.win)

                # Sensors
                if SHOW_SENSORS:
                    # This part works only on point sensors
                    for car in ACTIVE_CAR_GROUP:
                        for sensor in car.sensors:
                            # Break if not PointSensor
                            if isinstance(sensor, PointSensor):
                                col = RED if sensor.value else YELLOW
                                pygame.draw.circle(pygame.display.get_surface(), col, intify_tuple(sensor.position), 5)
                            elif isinstance(sensor, LineSensor):
                                col = sensor.color
                                pygame.draw.line(pygame.display.get_surface(), col,
                                                 sensor.rect.center, car.rect.center, 1)

                    LINE_SENSORS_GROUP.update()
                    LINE_SENSORS_GROUP.draw(self.win)

                # Backtrack markers for first car (showing all would be just a mess)
                if SHOW_BACKTRACK_MARKERS:
                    for car in ACTIVE_CAR_GROUP:
                        for marker in car.backtrack_markers:
                            pygame.draw.rect(self.win, YELLOWISH, marker)
                        break  # no need for other cars

                ACTIVE_CAR_GROUP.draw(self.win)

                if SHOW_BEST_CAR:
                    # best_car = None
                    for car in ACTIVE_CAR_GROUP:
                        if car.brain.points_this_round > self.highest_points_this_round:
                            self.highest_points_this_round = car.brain.points_this_round
                            self.best_car_this_round = car
                            # best_car = car
                            # self.best_car_last_known_pos = car.rect.center

                    if self.best_car_this_round is not None:
                        pygame.draw.circle(self.win, GREEN, self.best_car_this_round.rect.center, 24, 3)

                if SHOW_CAR_NUMBERS:
                    CAR_NUMBERS_GROUP.draw(self.win)

                # Show / hide infoblurb
                if hover_car is not None:
                    self.mouse_hover_info_blurb.add(UI_GROUP)
                else:
                    self.mouse_hover_info_blurb.remove(UI_GROUP)

                UI_GROUP.draw(self.win)

                # print(f"PointSensor values: {car.get_sensor_values()}")

                pygame.display.flip()

                if not self.fast_forward:
                    self.clock.tick(FPS if not self.slo_mo else 1)

        # Here if quit
        self.save_and_quit()

    # def add_brains_to_archive(self, car_list):
    #     for car in car_list:
    #         self.archive.add_brain_if_big_enough_score(car.brain)

    def save_and_quit(self):
        self.archive.save()
        self.archive.save_as_text()
        sys.exit(0)


if __name__ == '__main__':
    Program()



