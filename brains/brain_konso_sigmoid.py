import math
import random
from typing import Sequence
from brains.brain_base import BaseBrain, BrainOutputTuple, BrainInputTuple


class SigmoidBrain(BaseBrain):
    variance_amount = 0.4

    def __init__(self, number_of_inputs: int) -> None:
        BaseBrain.__init__(self, number_of_inputs)
        self._layers: list = []
        self._init_layers((number_of_inputs, 7, 4))

    def _init_layers(self, neurons_per_layer: tuple) -> None:
        self._layers = []
        for index, layer in enumerate(neurons_per_layer):
            if index == 0:
                # First 'layer' is only the input values - no neurons there
                self._layers.append([0 for _ in range(layer)])
            else:
                self._layers.append([SigmoidNeuron(number_of_inputs=len(self._layers[index - 1]),
                                                   variance_amount=SigmoidBrain.variance_amount) for _ in range(layer)])

        self._number_of_layers = len(self._layers)

    def _get_layer_output(self, inputs: Sequence[float]) -> Sequence[float]:
        for index, layer in enumerate(self._layers):
            # Every layer updates the inputs of the next layer
            if index == 0:
                for neuron in self._layers[index + 1]:
                    neuron.update_inputs(input_values=inputs)

            # Middle layers - not output layers
            elif index < self._number_of_layers - 1:
                layer_values = [n.get_value() for n in layer]
                for neuron in self._layers[index + 1]:
                    neuron.update_inputs(input_values=layer_values)

            # Output layer
            else:
                output = [n.get_value() for n in layer]

        for item in output:
            assert item >= 0, f"Brain {self.id} output less than zero: {output}"

        return output

    def make_decision(self, inputs: BrainInputTuple) -> BrainOutputTuple:
        output = self._get_layer_output(inputs)
        return BrainOutputTuple(*output)

    def evolve(self) -> None:
        for layer in self._layers[1:]:
            for neuron in layer:
                neuron.evolve()

    @property
    def author_name(self) -> str:
        return "Konso"


class SigmoidMemoryBrain(SigmoidBrain):
    """Has one memory output/input"""
    variance_amount = 0.4

    def __init__(self, number_of_inputs: int) -> None:
        BaseBrain.__init__(self, number_of_inputs)
        # Last input is for memory
        # Last output is for memory
        self._init_layers((number_of_inputs + 1, number_of_inputs + 1, 5))
        self._memory: float = 0

    def make_decision(self, inputs: BrainInputTuple) -> BrainOutputTuple:
        # Add memory value to inputs
        inputs = (*inputs, self._memory)

        output = self._get_layer_output(inputs)

        # Leave memory out from output
        output_tuple = BrainOutputTuple(*output[:4])
        # Save memory internally
        self._memory = output[4]

        return output_tuple


class KonsoTagBrain(SigmoidMemoryBrain):
    """Has one memory output/input"""
    variance_amount = 0.4

    def make_decision(self, inputs: BrainInputTuple) -> BrainOutputTuple:
        # Hoping these adjustments help the Brain to learn better
        adjusted_inputs = BrainInputTuple(
            car_speed=inputs.car_speed / 10,
            wall_sensor_right=inputs.wall_sensor_right / 10,
            wall_sensor_center=inputs.wall_sensor_center / 10,
            wall_sensor_left=inputs.wall_sensor_left / 10,
            radar_tag_distance=inputs.radar_tag_distance / 1000,
            radar_tag_angle=inputs.radar_tag_angle / 100,
            radar_non_tag_distance=inputs.radar_non_tag_distance / 1000,
            radar_non_tag_angle=inputs.radar_non_tag_angle / 100)

        return super().make_decision(adjusted_inputs)


class KonsoThreeLayerTagBrain(KonsoTagBrain):
    def __init__(self, number_of_inputs: int) -> None:
        super().__init__(number_of_inputs)
        # Last input is for memory
        # Last output is for memory
        self._init_layers((number_of_inputs + 1, number_of_inputs + 1, 7, 5))


class KonsoThreeLayerRaceBrain(KonsoThreeLayerTagBrain):
    def __init__(self, number_of_inputs: int) -> None:
        super().__init__(number_of_inputs)
        self.laps_started = 0
        self.laps_completed = 0


class ThreeLayerBrain(SigmoidBrain):
    variance_amount = 0.4

    def __init__(self, number_of_inputs: int):
        SigmoidBrain.__init__(self, number_of_inputs=number_of_inputs)
        neurons_per_layer = (number_of_inputs, number_of_inputs, 5, 4)
        self._layers = []
        for index, layer in enumerate(neurons_per_layer):
            if index == 0:
                self._layers.append([0 for _ in range(layer)])
            else:
                self._layers.append([SigmoidNeuron(number_of_inputs=len(self._layers[index - 1]),
                                                   variance_amount=self.variance_amount) for _ in range(layer)])

        self._number_of_layers = len(self._layers)


def logistic(x):
    try:
        return 1 / (1 + math.e**(x * -1))
    except OverflowError as e:
        # print(f"OverflowError in logistic - x: {x}")
        # print(e)
        # Dirty fix for too big values - does it work tho?
        return 1 if x > 0 else -1


def variance(amount):
    return random.uniform(-amount, amount)


class SigmoidNeuron:
    id_counter = 0

    def __init__(self, number_of_inputs: int, variance_amount: float):
        self.id = SigmoidNeuron.id_counter
        SigmoidNeuron.id_counter += 1

        self.variance_amount = variance_amount

        self.number_of_inputs = number_of_inputs
        self.input_values = [None for _ in range(number_of_inputs)]
        self.weights = [random.uniform(-1, 1) for _ in range(number_of_inputs)]
        self.bias = random.uniform(-1, 1)

        # print(f"Init {self}")

    def __repr__(self):
        return f"<SigmoidNeuron {self.id} - {self.number_of_inputs} inputs>"

    def update_inputs(self, input_values: iter):
        self.input_values = input_values

    def get_value(self):
        input_sum = 0
        for index, input_ in enumerate(self.input_values):
            input_sum += input_ * self.weights[index]

        output = logistic(input_sum + self.bias)
        # assert output >= 0, f"Neuron output value less than zero: {output}"
        # print(f"Neuron {self.id} output value: {output}")
        return output

    def evolve(self) -> None:
        # print(f"{self} - bias {self.bias} - weights {self.weights} - evolving")

        self.bias += variance(self.variance_amount)
        self.weights = [i + variance(self.variance_amount) for i in self.weights]
        # print(f"{self} - bias {self.bias} - weights {self.weights}")


if __name__ == '__main__':
    br = KonsoThreeLayerTagBrain(number_of_inputs=8)
    print(dir(br))
    for number, layer in enumerate(br._layers):
        print(number, len(layer), layer)
    print(br.make_decision(BrainInputTuple(1, 2, 3, 4, 5, 6, 7, 8)))