import copy
import pickle
from os.path import join
from abc import ABC, abstractmethod
from typing import Iterable, NamedTuple, Collection, Deque
from collections import namedtuple, defaultdict, deque
from operator import attrgetter
from datetime import datetime
from statistics import mean
from constants import *


class BaseBrain(ABC):
    id_counter = 0

    def __init__(self, number_of_inputs: int):
        # Brain ID
        self.id = self.get_new_id()

        # Number of inputs
        self.number_of_inputs = number_of_inputs

        self.name = self.__class__.__name__

        # Stats
        self.frames_alive = 0
        self.distance_traveled = 0
        self.rounds_started = 0
        self.laps_completed = 0
        self.crashes = 0
        self._last_rounds_points: Deque[float] = deque((0,), maxlen=20)
        # self._own_points = 0
        # self._ancestor_points = 0
        self.points_this_round = 0

        self.points_this_round_per_reason = dict()
        self._reset_points_this_round_per_reason()

        self.parent_id = None
        self.generation = 0
        self.ancestor_finished_levels = set()
        self.own_finished_levels = set()

        # print(f"Created a new {self.name} with id {self.id}")

    def __repr__(self) -> str:
        return f"<{self.__class__.__name__} {self.id} by {self.author_name} - " \
               f"points this round/avg: {int(self.points_this_round)}/{self.points}>"

    def _reset_points_this_round_per_reason(self) -> None:
        for reason_label in SCORE_REASONS:
            self.points_this_round_per_reason[reason_label] = 0

    def full_stats(self) -> str:
        return f"<{self.__class__.__name__} {self.id} (parent id {self.parent_id}) by {self.author_name} - " \
               f"generation {self.generation} - " \
               f"{self.rounds_started} / {self.laps_completed} laps started / completed - avg points {self.points} - " \
               f"last rounds' points: {self._last_rounds_points}>"
               # f"total points {int(self._ancestor_points + self._own_points)} - {int(self._own_points)} own points>"
        # f"finished levels own: {self.own_finished_levels}, ancestors: {self.ancestor_finished_levels}>"

    def _get_average_points(self) -> int:
        """Return average points per round started"""
        return mean(self._last_rounds_points)
        # points = self._own_points + self._ancestor_points
        # try:
        #     return points // self.rounds_started
        # except ZeroDivisionError:
        #     # No laps started yet
        #     return points
        #     return points // NO_LAPS_POINT_PENALTY_DIVISOR

    def add_points(self, amount: int, reason: str = REASON_UNDEFINED):
        # print(f"Brain {self.id} got {amount} points for reason {reason}.")
        self._last_rounds_points[-1] += amount
        self.points_this_round += amount
        try:
            self.points_this_round_per_reason[reason] += amount
        except KeyError:
            raise ValueError(f"Invalid score reason: {reason}. Must be in constants.py SCORE_REASONS.")

    def level_finished(self, level_name: str):
        self.own_finished_levels.add(level_name)

    def start_new_round(self) -> None:
        self.reset_points_this_round()
        self._last_rounds_points.append(0)
        self.rounds_started += 1
        # print(f"Brain {self.id} - new round - _last_rounds_points: {self._last_rounds_points}")

    def reset_points_this_round(self) -> None:
        self.points_this_round = 0
        self._reset_points_this_round_per_reason()

    def fix_stats_after_cloning(self):
        """ Sets ancestor/own stats properly. Should be called after cloning. """
        # self._ancestor_points = self._ancestor_points + self._own_points
        # self._own_points = 0

        # Don't clear points - keep parents' points
        # self._last_rounds_points.clear()
        # self._last_rounds_points.append(0)

        self._reset_points_this_round_per_reason()
        self.generation += 1
        self.ancestor_finished_levels = self.ancestor_finished_levels.union(self.own_finished_levels)
        self.own_finished_levels = set()

    def clone_and_evolve(self) -> 'BaseBrain':
        copy_ = copy.deepcopy(self)
        copy_.id = copy_.get_new_id()
        copy_.parent_id = self.id
        copy_.fix_stats_after_cloning()
        copy_.evolve()
        return copy_

    def save_to_file(self, directory: str = '.') -> None:
        now = datetime.now().isoformat().replace(":", ".")  # no ":" in file name
        filename = join(directory, f"brain_{self.author_name}_{self.__class__.__name__}_{self.id}_{now}.pkl")
        filename = filename.lower()
        print(f"Saving Brain to {filename}")
        with open(filename, 'wb') as f:
            pickle.dump(self, f)

    @staticmethod
    def load_from_file(filename: str) -> 'BaseBrain':
        print(f"Loading Brain from {filename}...")
        with open(filename, 'rb') as f:
            b = pickle.load(f)
        return b

    def process_sensor_readings(self, sensor_readings: Collection[float]) -> 'BrainOutputTuple':
        """Convert sensor readings to a BrainInputTuple and call self.make_decision with it
        CAR SHOULD CALL THIS METHOD
        Pass on the BrainOutputTuple that make_decision returns"""
        return self.make_decision(BrainInputTuple(*sensor_readings))

    @classmethod
    def get_new_id(cls) -> int:
        new_id = cls.id_counter
        cls.id_counter += 1
        return new_id

    @classmethod
    def sort_by_attribute(cls, input_, attribute):
        """ Returns the given list/whatever of Brains by given attribute, in descending order """
        _sort_key_function = attrgetter(attribute)
        return sorted(input_, key=_sort_key_function, reverse=True)

    # METHODS TO OVERWRITE:

    def evolve(self) -> None:
        """This method should be overridden. Is called on the child Brain after cloning. Should mutate
        Brain's internal state however the author wants."""
        pass

    @abstractmethod
    def make_decision(self, inputs: 'BrainInputTuple') -> 'BrainOutputTuple':
        """ This method should be overridden """
        pass

    @property
    @abstractmethod
    def author_name(self) -> str:
        """ This method should be overriden - should return author's name """
        return "Undefined"

    points = property(_get_average_points)
    score = property(_get_average_points)
    points_average = property(_get_average_points)


class ObstacleBrain(BaseBrain):
    """Always has very low score"""
    def __init__(self, number_of_inputs):
        super().__init__(number_of_inputs)
        self.points_this_round = -999999

    def make_decision(self, inputs: 'BrainInputTuple') -> 'BrainOutputTuple':
        return BrainOutputTuple(0, 0, 0, 0)

    @property
    def author_name(self) -> str:
        return "Konso"

    def _get_average_points(self) -> int:
        return -999999

    def reset_points_this_round(self) -> None:
        self.points_this_round = -999999

    points = property(_get_average_points)
    score = property(_get_average_points)
    points_average = property(_get_average_points)


class BrainOutputTuple(NamedTuple):
    accelerate: float
    brake: float
    turn_left: float
    turn_right: float


class BrainInputTuple(NamedTuple):
    car_speed: float
    wall_sensor_right: float
    wall_sensor_center: float
    wall_sensor_left: float
    radar_tag_distance: float
    radar_tag_angle: float
    radar_non_tag_distance: float
    radar_non_tag_angle: float


class RaceBrainInputTuple(BrainInputTuple):
    car_speed: float
    wall_sensor_right: float
    wall_sensor_center: float
    wall_sensor_left: float
    radar_tag_distance: float = 0
    radar_tag_angle: float = 0
    radar_non_tag_distance: float = 0
    radar_non_tag_angle: float = 0


if __name__ == '__main__':
    b = BaseBrain.load_from_file('..\\pickled_brains\\brain_konso_humantrainedbrain.pkl')

