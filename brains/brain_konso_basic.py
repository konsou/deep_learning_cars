from brains.brain_base import BaseBrain, BrainOutputTuple, BrainInputTuple


class BasicBrain(BaseBrain):
    """
    A hard-wired brain for 4 inputs (speed, left sensor, fron sensor, right sensor)
    Does not evolve
    NOTE: OLD, NON-WORKING - ONLY SUPPORTS WALL POINT SENSORS
    """
    def __init__(self, number_of_inputs) -> None:
        BaseBrain.__init__(self, number_of_inputs)

    def make_decision(self, inputs: BrainInputTuple) -> BrainOutputTuple:
        # Dumb brain - only handles 4 inputs
        speed, left, front, right = inputs[:4]
        accelerate, brake, turn_left, turn_right = False, False, False, False

        if left:
            turn_right = True
        if right:
            turn_left = True
        if front:
            brake = True

        if not left and not front and not right:
            accelerate = True

        return BrainOutputTuple(accelerate, brake, turn_left, turn_right)

    def evolve(self):
        # Basic brain can't evolve
        pass

    @property
    def author_name(self) -> str:
        return "Konso"


if __name__ == '__main__':
    br = BasicBrain(3)
    br.save_to_file(directory='../pickled_brains')


