from brains.brain_base import BaseBrain, BrainOutputTuple, BrainInputTuple


class DummyBrainStationary(BaseBrain):
    def __init__(self, number_of_inputs: int) -> None:
        BaseBrain.__init__(self, number_of_inputs)

    def make_decision(self, inputs: BrainInputTuple) -> BrainOutputTuple:
        output_tuple = BrainOutputTuple(accelerate=0,
                                        brake=0,
                                        turn_left=0,
                                        turn_right=0)

        return output_tuple

    def evolve(self) -> None:
        pass

    @property
    def author_name(self) -> str:
        return "Konso"


class DummyBrainLeftTurning(DummyBrainStationary):
    def make_decision(self, inputs: BrainInputTuple) -> BrainOutputTuple:
        return BrainOutputTuple(accelerate=0,
                                brake=0,
                                turn_left=1,
                                turn_right=0)