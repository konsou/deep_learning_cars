from brains.brain_base import BaseBrain, BrainInputTuple, BrainOutputTuple
from numpy import array
import pickle


class HumanTrainedBrain(BaseBrain):
    def __init__(self, number_of_inputs, model_file: str) -> None:
        BaseBrain.__init__(self, number_of_inputs)

        with open(model_file, 'rb') as f:
            self._model = pickle.load(f)

    @property
    def author_name(self) -> str:
        return "Konso"

    def make_decision(self, inputs: 'BrainInputTuple') -> 'BrainOutputTuple':
        input_array = array([inputs])  # Keras Model needs data in this form
        decisions = self._model.predict(input_array)
        # print(decisions[0])
        ret_val = BrainOutputTuple(*decisions[0])
        # print(ret_val)
        return ret_val


if __name__ == '__main__':
    b = HumanTrainedBrain(8)
    print(b.make_decision(BrainInputTuple(1, 50, 50, 40, 400, 30, 100, 0)))
    b.save_to_file('.')

    with open('brain_konso_humantrainedbrain_0_2019-11-16t10.32.43.888907.pkl', 'rb') as f:
        b2 = pickle.load(f)

    print(b2)





