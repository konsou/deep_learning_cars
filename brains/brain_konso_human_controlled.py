import pygame
from brains.brain_base import BaseBrain, BrainOutputTuple, BrainInputTuple
from pygame.locals import *


class HumanControlledBrain(BaseBrain):
    """This "brain" is controlled by arrow keys"""
    def __init__(self, number_of_inputs: int) -> None:
        BaseBrain.__init__(self, number_of_inputs)

    def make_decision(self, inputs: BrainInputTuple) -> BrainOutputTuple:
        keys_pressed = pygame.key.get_pressed()

        output_tuple = BrainOutputTuple(accelerate=keys_pressed[K_UP],
                                        brake=keys_pressed[K_DOWN],
                                        turn_left=keys_pressed[K_LEFT],
                                        turn_right=keys_pressed[K_RIGHT])

        return output_tuple

    def evolve(self) -> None:
        pass

    @property
    def author_name(self) -> str:
        return "Konso"


if __name__ == '__main__':
    b = HumanControlledBrain(8)
    b.save_to_file('../pickled_brains/')
