import pygame
from collections import namedtuple
from random import randint
from image_rotator import rotate_image_keep_center
from typing import Tuple, Optional
from groups import *
from colors import *
from constants import *


BrainInfo = namedtuple('BrainInfo', 'name id author_name')


class BaseLevel(pygame.sprite.Sprite):
    def __init__(self, name, image):
        pygame.sprite.Sprite.__init__(self)

        self.name = name

        self.image = pygame.image.load(image).convert_alpha()
        self.image_filename = image

        self.rect = self.image.get_rect()
        self.mask = pygame.mask.from_surface(self.image)

    def __repr__(self):
        return f"<Level - {self.name} {self.image_filename}>"

    def get_at(self, position: tuple):
        try:
            return self.image.get_at(position)
        except IndexError:
            # Return full opaque white if point is outside level area
            return 255, 255, 255, 255

    def get_valid_starting_position(self, placement_tries: int, is_tag: bool = False,
                                    car_size: Tuple[int, int] = (32, 32)) -> Tuple[int, int]:
        """Return coordinates that are not inside a wall."""
        if placement_tries < 1:
            raise ValueError("Must have at least 1 placement try")

        width = self.image.get_width()
        height = self.image.get_height()

        car_rect = pygame.Rect(0, 0, car_size[0], car_size[1])

        for _ in range(placement_tries):
            pos = randint(0, width - 1), randint(0, height - 1)
            car_rect.center = pos

            # Full alpha / zero opacity: found valid coordinates
            # Check car rect's all corners
            if self.get_at(car_rect.topleft)[3] == \
                    self.get_at(car_rect.topright)[3] == \
                    self.get_at(car_rect.bottomleft)[3] == \
                    self.get_at(car_rect.bottomright)[3] == 0:
                return pos

            # if self.get_at(pos)[3] == 0:
            #     return pos

        # Here if didn't find a valid position in allotted tries - just return the last value
        return pos

    def reset_starting_positions(self) -> None:
        """Called after round is completed - randomize new starting position on TagTrainingLevel"""
        pass


class TagTrainingLevel(BaseLevel):
    def __init__(self, name: str, image: str,
                 tag_starting_position: Optional[Tuple[int, int]],
                 non_tag_starting_position: Optional[Tuple[int, int]],
                 tag_start_heading: Optional[float] = None,
                 non_tag_start_heading: Optional[float] = None,
                 tag_starting_position_keep_same: bool = False,
                 non_tag_starting_position_keep_same: bool = False,
                 obstacle_tag_cars: Optional[Tuple[Tuple[int, int], ...]] = None,
                 obstacle_non_tag_cars: Optional[Tuple[Tuple[int, int], ...]] = None,
                 keep_spawning_non_tags: bool = False) -> None:
        BaseLevel.__init__(self, name=name, image=image)

        self.tag_starting_position: Tuple[int, int] = tag_starting_position
        self.tag_start_heading: float = tag_start_heading
        self.tag_starting_position_keep_same: bool = tag_starting_position_keep_same
        self.non_tag_starting_position_keep_same: bool = non_tag_starting_position_keep_same
        self.non_tag_starting_position: Tuple[int, int] = non_tag_starting_position
        self.non_tag_start_heading: float = non_tag_start_heading

        self.obstacle_tag_cars: Optional[Tuple[Tuple[int, int]]] = obstacle_tag_cars
        self.obstacle_non_tag_cars: Optional[Tuple[Tuple[int, int]]] = obstacle_non_tag_cars

        self.keep_spawning_non_tags: bool = keep_spawning_non_tags

    def get_valid_starting_position(self, placement_tries: int, is_tag: bool = False,
                                    car_size: Tuple[int, int] = (32, 32)) -> Tuple[int, int]:
        """Return predefined starting coordinates if they're set
        If not, return random starting coordinates"""
        # print(f"In get_valid_starting_position - is_tag: {is_tag}")
        if is_tag:
            if self.tag_starting_position is None:
                # Static starting position not set - return a random position
                ret_val = super().get_valid_starting_position(placement_tries=placement_tries,
                                                              is_tag=is_tag,
                                                              car_size=car_size)

                # Save the generated starting position so all cars this round will have the same position
                if self.tag_starting_position_keep_same:
                    self.tag_starting_position = ret_val
            else:
                ret_val = self.tag_starting_position
        else:
            if self.non_tag_starting_position is None:
                # Static starting position not set - return a random position
                ret_val = super().get_valid_starting_position(placement_tries=placement_tries,
                                                              is_tag=is_tag,
                                                              car_size=car_size)

                # Save the generated starting position so all cars this round will have the same position
                if self.non_tag_starting_position_keep_same:
                    self.non_tag_starting_position = ret_val
            else:
                ret_val = self.non_tag_starting_position

        return ret_val

    def reset_starting_positions(self) -> None:
        """Reset saved starting positions - next round positions will be re-randomized"""
        if self.tag_starting_position_keep_same:
            self.tag_starting_position = None
        if self.non_tag_starting_position_keep_same:
            self.non_tag_starting_position = None


class FakeLevel(BaseLevel):
    """Fake Level for testing purposes"""
    def __init__(self):
        BaseLevel.__init__(self, name="FakeLevel", image='images/black_pixel.png')


class Level(BaseLevel):
    def __init__(self, name, image, start_pos, start_direction, laps, lap_minimum_distance, finish_line_width=80,
                 finish_line_thickness=10):
        BaseLevel.__init__(self, name, image)

        self.lap_minimum_distance = lap_minimum_distance

        self.start_pos = start_pos
        self.start_direction = start_direction
        self.laps = laps

        self.fastest_lap_frames = 99999999999999999
        self.fastest_lap_brain: BrainInfo = None

        self.finish_line = FinishLine(start_pos, start_direction + 90,
                                      width=finish_line_width, thickness=finish_line_thickness)

    def add_fastest_lap_if_applicable(self, frames_taken: int, brain):
        if frames_taken < self.fastest_lap_frames:
            print(f"LAP RECORD! {frames_taken} frames by {brain}")
            self.fastest_lap_frames = frames_taken
            self.fastest_lap_brain = BrainInfo(name=brain.__class__.__name__, id=brain.id, author_name=brain.author_name)
            with open(STATS_DUMP_FILENAME, 'a') as f:
                f.write(f"LAP RECORD in {self.name}! {frames_taken} frames by {brain}")


class FinishLine(pygame.sprite.Sprite):
    def __init__(self, pos, rotation, width=80, thickness=10):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((100, 100)).convert_alpha()
        self.image.fill(TRANSPARENCY)
        pygame.draw.rect(self.image, GREEN, (width // 2 - thickness // 2, 0, thickness, width))
        self.image = rotate_image_keep_center(self.image, rotation)
        self.rect = self.image.get_rect()
        self.rect.center = pos

        pygame.draw.circle(self.image, YELLOW, self.rect.center, 5)

        self.mask = pygame.mask.from_surface(self.image)
        # print(f"Finish line rect is {self.rect}")


if __name__ == '__main__':
    pygame.init()
    win = pygame.display.set_mode((1000, 600))
    a = TagTrainingLevel(name='Corridor to the right', image='levels/training_corridor.png',
                         tag_starting_position=(133, 313), non_tag_starting_position=(648, 315))

    print(a)

    print(a.get_valid_starting_position(True))
    print(a.get_valid_starting_position(False))

