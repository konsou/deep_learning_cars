from os.path import join

ACCELERATE, BRAKE, TURN_LEFT, TURN_RIGHT = range(4)
TAG, NON_TAG = range(2)
MODE_TRAINING, MODE_COMPETITION = range(2)

# THESE AFFECT MAINLY THE RACING SIM IN main.py
# TAG SIM IN tag.py USES (MOSTLY) ITS OWN CONSTANTS

# TODO: Make tag.py use its own constants everywhere

# Program behaviour
FPS = 30
FOREVER_TRAINING_MODE = True
LOOP_LEVEL_UNTIL_CAR_FINISHES = False
SHOW_SENSORS = False
SHOW_CAR_NUMBERS = True
SHOW_SCORES_ON_CARS = False
SHOW_BACKTRACK_MARKERS = True
SHOW_FUEL_GAUGE = False
SHOW_BEST_CAR = True
DRAW_GRAPHICS = True

BRAIN_ARCHIVE_PICKS_PER_ROUND = 5
CLONES_PER_ARCHIVE_PICK = 9
NUMBER_OF_CARS = BRAIN_ARCHIVE_PICKS_PER_ROUND * (CLONES_PER_ARCHIVE_PICK + 1)

BRAIN_ARCHIVE_SIZE = 30

# Car characteristics
# FEATURE FREEZE - DON'T CHANGE!
CAR_FUEL = 2500  # How many pixels approximately car can move - 2000 should be enough?
CAR_MAX_SPEED = 10
CAR_MIN_SPEED = 1
CAR_TURNING_SPEED = 20
CAR_ACCELERATION = 1
CAR_BRAKING = 1

# How accurate sensors are - smaller -> more accurate
SENSOR_RESOLUTION = 2

# Lap validity checking
FINISH_LINE_ALLOWED_HEADING_DIFFERENCE = 90

# SENSORS
LINEAR_SENSOR_RANGE = 300

# These affect brain evolution selection
POINTS_PER_LIFETIME_TICK = -1  # Encourage fast driving
# POINTS_PER_DISTANCE_TRAVELED is NOT awarded if lap is completed
# This encourages efficient route selection
POINTS_PER_DISTANCE_TRAVELED = 2
POINTS_PER_CRASH = -500
POINTS_PER_LAP = 10000
# Backtrack markers
# Every x frames car leaves behind a hidden, rectangular marker
# If the car collides with a marker it's left behind it gets a penalty
BACKTRACK_PENALTY_PER_MARKER = -100
BACKTRACK_MARKER_LEAVE_INTERVAL = 15
BACKTRACK_MARKER_SIZE = 10
BACTRACK_MARKERS_SAVED = 15  # this many backtrack markers are saved - older ones are discarded

# NO_LAPS_POINT_PENALTY_DIVISOR = 10
# POINTS_PER_FINISH_POSITION = {
#     0: 1000,
#     1: 500,
#     2: 250,
#     3: 100,
#     4: 50,
# }

# Used for Brain score reasons
REASON_LIFETIME = "LIFETIME"
REASON_DISTANCE = "DISTANCE"
REASON_LAP = "LAP"
REASON_CRASH = "CRASH"
REASON_TAG_CATCH = "TAG CATCH"
REASON_WIN_ROUND = "WIN ROUND"
REASON_NEAR_NON_TAG_CAR = "NEAR NON-TAG"
REASON_BACKTRACK = "BACKTRACK"
REASON_UNDEFINED = "(undefined)"
SCORE_REASONS = (REASON_LIFETIME, REASON_DISTANCE, REASON_LAP, REASON_CRASH, REASON_TAG_CATCH,
                 REASON_WIN_ROUND, REASON_NEAR_NON_TAG_CAR,
                 REASON_BACKTRACK, REASON_UNDEFINED)

# File and directory names
BRAIN_ARCHIVE_DIRECTORY = 'pickled_brains'
STATS_DIRECTORY = 'stats'
SENSOR_DATA_RECORDING_DIRECTORY = 'sensor_data'

BRAIN_ARCHIVE_FILENAME = join(BRAIN_ARCHIVE_DIRECTORY, 'pickled_brains.pkl')
STATS_DUMP_FILENAME = join(STATS_DIRECTORY, 'stats_dump.txt')
