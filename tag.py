import sys
import os
import multiprocessing
import json
import copy
import pickle
from pygame.sprite import Group
from random import randint
from datetime import datetime
from statistics import mean
from typing import List, Type, Optional
from os.path import join, isfile
from level import BaseLevel, TagTrainingLevel
from car import TagCar
from sensor import SpeedSensorTemplate, CarRadarSensorTemplate, TagLineSensorTemplate, LineSensorTemplate
from sensor import ZeroReturningCarRadarSensorTemplate
from sensor import SpeedSensor, CarRadarSensor, TagLineSensor, LineSensor, PointSensor
from sensor import ZeroReturningCarRadarSensor
from utility import intify_tuple, DotDict
from brains.brain_base import BaseBrain, ObstacleBrain, BrainInputTuple
from brains.brain_konso_sigmoid import SigmoidBrain, ThreeLayerBrain, SigmoidMemoryBrain, KonsoThreeLayerTagBrain
from brains.brain_konso_sigmoid import KonsoTagBrain
from brains.brain_konso_human_controlled import HumanControlledBrain
from brains.brain_human_trained import HumanTrainedBrain
from brains.brain_dummy import DummyBrainStationary
from brains.brain_konso_basic import BasicBrain
from brain_archive import BrainArchive, load_archive
from text import Text
from ui import *
from colors import *
from groups import *
from constants import *
from pygame.locals import *

# FOR TRAINING MODE: TYPES OF BRAINS TO USE WHEN SPAWNING NEW BRAINS
TRAINING_AVAILABLE_BRAIN_TYPES_TAG: List[Type[BaseBrain]] = [KonsoThreeLayerTagBrain]
TRAINING_AVAILABLE_BRAIN_TYPES_NON_TAG: List[Type[BaseBrain]] = [DummyBrainStationary]

COMPETITION_BRAIN_INSTANCES_TAG: List[BaseBrain] = [
    BaseBrain.load_from_file(
        'pickled_brains/brain_konso_konsothreelayertagbrain_497028_2019-12-17t19.35.26.032409.pkl'),
]
COMPETITION_BRAIN_INSTANCES_NON_TAG: List[BaseBrain] = [
                # BaseBrain.load_from_file('pickled_brains/brain_konso_humancontrolledbrain.pkl'),
                BaseBrain.load_from_file('pickled_brains/brain_konso_humantrainedbrain_0_2019-12-18t16.27.36.997389.pkl'),
            ]


class Program(multiprocessing.Process):
    def __init__(self, is_subprocess=False,
                 command_queue: multiprocessing.JoinableQueue = None,
                 result_queue: multiprocessing.Queue = None) -> None:
        multiprocessing.Process.__init__(self)
        self.is_subprocess = is_subprocess
        pid = os.getpid()

        # PROGRAM SETTINGS
        self.SETTINGS_FILENAME: str = "settings_tag.json"

        self._default_settings: dict = {
            # FEATURE FREEZE - DON'T CHANGE NON-TAG VALUES!
            'BRAIN_REWARD_VALUES': {
                'POINTS_PER_LIFETIME_TICK': 1,
                'POINTS_PER_CRASH': -500,
                'POINTS_PER_TAG_CATCH': 1000,
                'POINTS_FOR_ROUND_WIN': 500,

                'POINTS_PER_LIFETIME_TICK_TAG': 0,
                # on death, tag is given points: (points_per_tag_catch / 2) - (distance_to_non_tag * points_deduct)
                'POINTS_DEDUCT_PER_DISTANCE_TO_NON_TAG': 1,
                },

            # TRAINING / COMPETITION MODE
            # Stationary cars are killed off in training mode
            'mode': MODE_TRAINING,
            'competition_rounds': 10,
            'non_tag_training_mode': False,  # no tag car if this is set
            'save_sensor_data': False,  # Save all cars' sensor readings to file EVERY FRAME

            'draw_graphics': True,
            'show_sensors:': False,
            'sensor_resolution': 2,  # move this many pixels on every step
            'line_sensor_range': 50,

            # CAR STUFF
            'tag_cars': 50,
            'non_tag_cars': 1,
            'tag_cars_collide': True,
            'stationary_tag_kill_probability': 1,  # probability of killing a random stationary car every frame
            'stationary_non_tag_kill_probability': 1,  # probability of killing a random stationary car every frame
            'stationary_frames_kill_treshold': 60,  # how many frames car needs to be stationary to be possibly killed

            # CAR PROPERTIES
            'car_fuel': 1500,
            'car_max_speed': 10,
            'car_min_speed': 0,
            # self.settings.car_turning_speed = 20
            # self.settings.car_acceleration = 1
            'car_turning_speed': 10,
            'car_acceleration': 0.5,
            'car_braking': 1,
            'car_friction': 0.01,  # speed = (1 - friction) * speed

            # BRAIN ARCHIVING AND SELECTION
            'use_archive': True,  # only save the best brains each round if this is False
            # The rest are only used in training mode and when using archives
            'tag_archive_size': 50,
            'non_tag_archive_size': 50,
            'percentage_cars_survive': 1,
            'tag_archive_picks': 5,
            'tag_clones_per_archive_pick': 10,
            'non_tag_archive_picks': 1,
            'non_tag_clones_per_archive_pick': 0,

            # FILENAMES
            'best_tag_brain_filename': 'pickled_brains/best_brain_tag.pkl',
            'best_non_tag_brain_filename': 'pickled_brains/best_brain_non_tag.pkl',
            'stats_directory': 'stats',
        }
        self.settings: DotDict

        # LOAD SETTINGS
        try:
            with open(self.SETTINGS_FILENAME) as f:
                settings = json.load(f)
                self.settings = DotDict(settings)
        except (OSError, KeyError):
            print(f"Error reading settings file {self.SETTINGS_FILENAME} - using default settings")
            settings = copy.copy(self._default_settings)
            self.settings = DotDict(settings)
            self._save_settings()

        if self.settings.mode == MODE_COMPETITION:
            self.COMPETITION_BRAIN_INSTANCES_TAG: List[BaseBrain] = COMPETITION_BRAIN_INSTANCES_TAG
            self.COMPETITION_BRAIN_INSTANCES_NON_TAG: List[BaseBrain] = COMPETITION_BRAIN_INSTANCES_NON_TAG

        if is_subprocess:
            self.command_queue = command_queue
            self.result_queue = result_queue

        # AUDIO INIT
        pygame.mixer.pre_init(frequency=22050, size=-16, channels=2, buffer=1024)
        pygame.init()

        # GRAPHICS INIT
        self.win = pygame.display.set_mode((1200, 600))
        self.game_area_rect: Rect = Rect(0, 0, 800, 600)
        self.info_area_rect: Rect = Rect(800, 0, 400, 600)
        pygame.display.set_caption(f"Deep learning cars TAG - {pid}")

        print(f"Process ID is {pid}")
        self.clock = pygame.time.Clock()

        self.running: bool = False
        self.fast_forward: bool = False
        self.paused: bool = False
        self.slo_mo: bool = False
        self.new_round_just_started: bool = True
        self.round_number: int = 0
        self.best_car_last_known_pos: Tuple[int, int] = (-100, -100)
        self.highest_points_this_round: float = 0
        self.best_car_this_round: Optional[TagCar] = None
        self.last_alive_non_tag_car: Optional[TagCar] = None

        # TODO: MOVE ALL SETTINGS TO JSON FILE
        if not os.path.isdir(self.settings.stats_directory):
            print(f"Creating stats directory: {self.settings.stats_directory}")
            os.makedirs(self.settings.stats_directory)

        self.TAG_ARCHIVE_NAME = "TAG"
        self.NON_TAG_ARCHIVE_NAME = "NON-TAG"
        self.TAG_ARCHIVE_FILENAME = join(BRAIN_ARCHIVE_DIRECTORY, "pickled_brains_tag.pkl")
        self.NON_TAG_ARCHIVE_FILENAME = join(BRAIN_ARCHIVE_DIRECTORY, "pickled_brains_non_tag.pkl")
        self.TAG_ARCHIVE_STATS_DUMP_FILENAME = join(STATS_DIRECTORY, "stats_dump_tag.txt")
        self.NON_TAG_ARCHIVE_STATS_DUMP_FILENAME = join(STATS_DIRECTORY, "stats_dump_non_tag.txt")
        self.AGGREGATE_STATS_FILENAME = join(STATS_DIRECTORY, "stats_aggregate_tag.txt")
        self.TAG_SENSOR_DATA_FILENAME = join(SENSOR_DATA_RECORDING_DIRECTORY, "tag_sensor_data.csv")
        self.NON_TAG_SENSOR_DATA_FILENAME = join(SENSOR_DATA_RECORDING_DIRECTORY, "non_tag_sensor_data.csv")

        # Try this many times to find a non-crashing placement for every car
        self.CAR_PLACEMENT_TRIES = 100

        # SPRITE GROUPS
        self.all_active_cars_group: Group = Group()
        self.tag_cars_group: Group = Group()
        self.non_tag_cars_group: Group = Group()
        self.obstacle_cars_group: Group = Group()
        self.inactive_cars_tag: Group = Group()
        self.inactive_cars_non_tag: Group = Group()
        self.crashed_cars: Group = Group()
        self.sensors_group: Group = Group()

        # Sensor templates
        self.sensor_templates = [
            SpeedSensorTemplate(label="Speed"),
            LineSensorTemplate(label="Wall left",
                               angle=45, distance=self.settings.line_sensor_range,
                               sensor_resolution=self.settings.sensor_resolution,
                               show_sensors=self.settings.show_sensors),
            LineSensorTemplate(label="Wall center",
                               angle=0, distance=self.settings.line_sensor_range,
                               sensor_resolution=self.settings.sensor_resolution,
                               show_sensors=self.settings.show_sensors),
            LineSensorTemplate(label="Wall right",
                               angle=-45, distance=self.settings.line_sensor_range,
                               sensor_resolution=self.settings.sensor_resolution,
                               show_sensors=self.settings.show_sensors),

            # TODO: CHECK THIS!!!
            CarRadarSensorTemplate(label="Tag radar", scan_group=self.tag_cars_group),
            CarRadarSensorTemplate(label="Non-tag radar", scan_group=self.non_tag_cars_group),
        ]

        # Calculate number of sensor values
        sensor_outputs = 0
        for sensor_template_ in self.sensor_templates:
            sensor_outputs += sensor_template_.number_of_outputs

        self.number_of_sensors = sensor_outputs

        # UI
        self.info_blurb_car = None
        info_data = {
                        "ID": 0,
                        "This round": 0,
                        "Avg score": 0,
                        "Fuel": 0,
                    }

        for reason in SCORE_REASONS:
            info_data[reason] = 0

        info_data['Actual speed'] = 0

        for field in BrainInputTuple._fields:
            info_data[field] = 0

        info_data['ACCELERATE'] = 0
        info_data['BRAKE'] = 0
        info_data['LEFT'] = 0
        info_data['RIGHT'] = 0
        info_data['Memory'] = 0

        self.info_blurb = InfoBlurb(info_data=info_data,
                                    position_coordinates=(810, 10),
                                    position_code=TOP_LEFT,
                                    )

        # Levels
        self.available_levels = [
            # BaseLevel(name='Basic Tag', image='levels/tag_level1.png'),
            # BaseLevel(name='Donut', image='levels/level1.png'),
            BaseLevel(name='Pillars', image='levels/tag_level2.png'),

            # TagTrainingLevel(name='Corridor to the right', image='levels/training_corridor.png',
            #                  tag_starting_position=(133, 313), non_tag_starting_position=(648, 315),
            #                  tag_start_heading=270, non_tag_start_heading=270),

            # TagTrainingLevel(name='Corridor to the left', image='levels/training_corridor.png',
            #                  tag_starting_position=(648, 315), non_tag_starting_position=(133, 313),
            #                  tag_start_heading=90, non_tag_start_heading=270),

            # TagTrainingLevel(name='Twisted corridor to the right', image='levels/training_corridor_2.png',
            #                  tag_starting_position=(133, 313), non_tag_starting_position=(648, 315),
            #                  tag_start_heading=270, non_tag_start_heading=270),
            #
            # TagTrainingLevel(name='Twisted corridor to the left', image='levels/training_corridor_2.png',
            #                  tag_starting_position=(648, 315), non_tag_starting_position=(133, 313),
            #                  tag_start_heading=90, non_tag_start_heading=270),
            #
            # # 4
            # TagTrainingLevel(name='Branching corridor to the right', image='levels/training_corridor_3.png',
            #                  tag_starting_position=(133, 313), non_tag_starting_position=(648, 315),
            #                  tag_start_heading=270, non_tag_start_heading=270),
            #
            # # 5
            # TagTrainingLevel(name='Y-corridor - non-tag in bottom', image='levels/training_corridor_y.png',
            #                  tag_starting_position=(133, 313), non_tag_starting_position=(523, 254),
            #                  tag_start_heading=270, non_tag_start_heading=270),
            # # TagTrainingLevel(name='Y-corridor - non-tag in middle', image='levels/training_corridor_y.png',
            # #                  tag_starting_position=(133, 313), non_tag_starting_position=(648, 315),
            # #                  tag_start_heading=270, non_tag_start_heading=270),
            # TagTrainingLevel(name='Y-corridor - non-tag in top', image='levels/training_corridor_y.png',
            #                  tag_starting_position=(133, 313), non_tag_starting_position=(521, 384),
            #                  tag_start_heading=270, non_tag_start_heading=270),
            #
            # TagTrainingLevel(name='Open', image='levels/tag_level1.png',
            #                  tag_starting_position=(133, 313), non_tag_starting_position=(648, 315),
            #                  tag_start_heading=0, non_tag_start_heading=270),
            #
            # TagTrainingLevel(name='Open 2', image='levels/tag_level1.png',
            #                  tag_starting_position=(648, 315), non_tag_starting_position=(133, 313),
            #                  tag_start_heading=0, non_tag_start_heading=270),
            #
            # TagTrainingLevel(name='Open 2 - random start pos', image='levels/tag_level1.png',
            #                  tag_starting_position=(400, 300), non_tag_starting_position=None,
            #                  tag_start_heading=0, non_tag_start_heading=270,
            #                  keep_spawning_non_tags=False),
            #
            # TagTrainingLevel(name='Donut 1', image='levels/level1.png',
            #                  tag_starting_position=(160, 300), non_tag_starting_position=(687, 223),
            #                  tag_start_heading=180, non_tag_start_heading=270),
            #
            # TagTrainingLevel(name='Donut 2', image='levels/level1.png',
            #                  tag_starting_position=(687, 223), non_tag_starting_position=(160, 300),
            #                  tag_start_heading=180, non_tag_start_heading=270),
            #
            # TagTrainingLevel(name='Donut random', image='levels/level1.png',
            #                  tag_starting_position=None, non_tag_starting_position=None,
            #                  tag_start_heading=180, non_tag_start_heading=270,
            #                  tag_starting_position_keep_same=True),
            #
            # TagTrainingLevel(name='Dodge the tag right - go straight', image='levels/training_corridor_bulge.png',
            #                  tag_starting_position=(133, 313), non_tag_starting_position=(648, 315),
            #                  tag_start_heading=270, non_tag_start_heading=270,
            #                  obstacle_tag_cars=((500, 290), (280, 350))),
            # TagTrainingLevel(name='Dodge the tag left - go straight', image='levels/training_corridor_bulge.png',
            #                  tag_starting_position=(648, 315), non_tag_starting_position=(133, 313),
            #                  tag_start_heading=90, non_tag_start_heading=270,
            #                  obstacle_tag_cars=((500, 290), (280, 350))),
            # TagTrainingLevel(name='Dodge the tag left - swerve', image='levels/training_corridor_bulge.png',
            #                  tag_starting_position=(648, 315), non_tag_starting_position=(133, 313),
            #                  tag_start_heading=90, non_tag_start_heading=90,
            #                  obstacle_tag_cars=((510, 325), (275, 310))),
            #
            # # 13
            # TagTrainingLevel(name='Dodge the tag right - swerve', image='levels/training_corridor_bulge.png',
            #                  tag_starting_position=(133, 313), non_tag_starting_position=(648, 315),
            #                  tag_start_heading=270, non_tag_start_heading=90,
            #                  obstacle_tag_cars=((510, 325), (275, 310))),
            #
            # TagTrainingLevel(name='Dodge the tag open', image='levels/tag_level1.png',
            #                  tag_starting_position=(235, 275), non_tag_starting_position=(635, 280),
            #                  tag_start_heading=270, non_tag_start_heading=270,
            #                  obstacle_tag_cars=((400, 275), (400, 250)),
            #                  keep_spawning_non_tags=False),
            #
            # TagTrainingLevel(name='Dodge the tag open random', image='levels/tag_level1.png',
            #                  tag_starting_position=(235, 275), non_tag_starting_position=None,
            #                  tag_start_heading=270, non_tag_start_heading=270,
            #                  obstacle_tag_cars=((400, 275), (400, 250)),
            #                  keep_spawning_non_tags=True),
            #
            # TagTrainingLevel(name='Pillars random', image='levels/tag_level2.png',
            #                  tag_starting_position=None, non_tag_starting_position=None,
            #                  tag_start_heading=180, non_tag_start_heading=270,
            #                  tag_starting_position_keep_same=True,
            #                  obstacle_tag_cars=((325, 280), (565, 87), (511, 248), (145, 410), (485, 485),
            #                                     (230, 110)),
            #                  keep_spawning_non_tags=True),

        ]

        # self.available_levels = [self.available_levels[2], self.available_levels[-1], self.available_levels[-1], self.available_levels[-1]]
        # self.available_levels = [self.available_levels[17]]
        # self.available_levels = self.available_levels[2:4]  # twisty corridors
        # self.available_levels = self.available_levels[11:15]  # dodge the tag
        # self.available_levels = [self.available_levels[5]]
        # self.available_levels = [self.available_levels[5], self.available_levels[6], self.available_levels[5], self.available_levels[6]]  # y
        # self.available_levels = self.available_levels[5:8]  # catch the greens
        # self.available_levels = self.available_levels[5:8] + self.available_levels[7:8] + self.available_levels[11:15]  # catch cgreens, dodge tags
        # self.available_levels = self.available_levels[14:17]
        # self.available_levels = [self.available_levels[-1]]

        self.level: Optional[BaseLevel] = None
        self._current_level_index: int = -1
        self.select_next_level()

        # Archives - load from disk if possible
        if self.settings.use_archive:
            try:
                self.tag_archive: Optional[BrainArchive] = load_archive(filename=self.TAG_ARCHIVE_FILENAME)
                self.tag_archive.change_max_size(self.settings.tag_archive_size)
            except OSError:
                print(f"Loading failed - creating a new empty tag archive...")
                self.tag_archive: Optional[BrainArchive] = BrainArchive(name=self.TAG_ARCHIVE_NAME,
                                                                        max_size=self.settings.tag_archive_size,
                                                                        filename=self.TAG_ARCHIVE_FILENAME,
                                                                        stats_dump_filename=self.TAG_ARCHIVE_STATS_DUMP_FILENAME)
            try:
                self.non_tag_archive: Optional[BrainArchive] = load_archive(filename=self.NON_TAG_ARCHIVE_FILENAME)
                self.non_tag_archive.change_max_size(self.settings.non_tag_archive_size)
            except OSError:
                print(f"Loading failed - creating a new empty non-tag archive...")
                self.non_tag_archive: Optional[BrainArchive] = BrainArchive(name=self.NON_TAG_ARCHIVE_NAME,
                                                                            max_size=self.settings.non_tag_archive_size,
                                                                            filename=self.NON_TAG_ARCHIVE_FILENAME,
                                                                            stats_dump_filename=self.NON_TAG_ARCHIVE_STATS_DUMP_FILENAME)
            # Set BaseBrain id counter properly
            highest_id = max(self.tag_archive.highest_id_in_use,
                             self.non_tag_archive.highest_id_in_use)
            BaseBrain.id_counter = highest_id + 1
            print(f"BaseBrain id counter set to {BaseBrain.id_counter}")

        else:  # no archive - only store the best brain for each round
            self.tag_archive: Optional[BrainArchive] = None
            self.non_tag_archive: Optional[BrainArchive] = None

            if isfile(self.settings.best_tag_brain_filename):
                with open(self.settings.best_tag_brain_filename, 'rb') as f:
                    self.best_tag_brain_store: List[BaseBrain] = pickle.load(f)
            else:
                self.best_tag_brain_store: List[BaseBrain] = []
                
            if isfile(self.settings.best_non_tag_brain_filename):
                with open(self.settings.best_non_tag_brain_filename, 'rb') as f:
                    self.best_non_tag_brain_store: List[BaseBrain] = pickle.load(f)
            else:
                self.best_non_tag_brain_store: List[BaseBrain] = []

        self.create_cars_and_brains()

        if not self.is_subprocess:
            self.run()

    def _save_settings(self) -> None:
        print(f"Saving settings to {self.SETTINGS_FILENAME}...")
        with open(self.SETTINGS_FILENAME, 'w') as f:
            json.dump(self.settings, f, indent=4)

    def select_next_level(self):
        # print(f"Selecting next level...")
        self._select_level(self._current_level_index + 1)

    def _select_level(self, level_number: int):
        print(f"Selecting level {level_number}...")
        self._current_level_index = level_number

        if self.level is not None:
            self.level.kill()
        self.level = self.available_levels[self._current_level_index]
        self.level.add(LEVEL_GROUP)

        self.level.reset_starting_positions()
        # print(f"Selected level {self.level}")

    def create_cars_and_brains(self):
        if self.settings.mode == MODE_COMPETITION and self.round_number >= self.settings.competition_rounds:
            print(f"ALL ROUNDS COMPLETE!")
            self.save_and_quit()
        print(f"---------------NEW ROUND---------------")
        # input(f"Press ENTER to continue...")
        # print(f"Clear current cars...")
        self.round_number += 1
        self.all_active_cars_group.empty()
        self.tag_cars_group.empty()
        self.non_tag_cars_group.empty()
        self.inactive_cars_tag.empty()
        self.inactive_cars_non_tag.empty()
        self.crashed_cars.empty()
        self.info_blurb_car = None
        self.new_round_just_started = True
        if self.settings.mode == MODE_COMPETITION:
            print(f"PRESS ENTER TO START ROUND {self.round_number}")

        # print(f"Spawning brains")

        self.highest_points_this_round = 0
        self.best_car_this_round = None

        non_tag, tag = 0, 1
        archive = {
            non_tag: self.non_tag_archive,
            tag: self.tag_archive,
        }
        archive_picks = {
            non_tag: self.settings.non_tag_archive_picks,
            tag: self.settings.tag_archive_picks,
        }
        cars = {
            non_tag: self.settings.non_tag_cars,
            tag: self.settings.tag_cars,
        }
        group_active = {
            non_tag: self.non_tag_cars_group,
            tag: self.tag_cars_group
        }
        group_inactive = {
            non_tag: self.inactive_cars_non_tag,
            tag: self.inactive_cars_tag
        }
        available_brains = {
            non_tag: TRAINING_AVAILABLE_BRAIN_TYPES_NON_TAG,
            tag: TRAINING_AVAILABLE_BRAIN_TYPES_TAG,
        }
        text = {
            non_tag: "NON_TAG",
            tag: "TAG",
        }
        clones_per_archive_pick = {
            non_tag: self.settings.non_tag_clones_per_archive_pick,
            tag: self.settings.tag_clones_per_archive_pick,
        }

        for round_ in tag, non_tag:
            # print(f"ROUND: {round_}")
            # no tag cars if non-tag training
            if self.settings.non_tag_training_mode:
                if round_ == tag:
                    continue

            # REMEMBERING TO CLEAR selected_brains!!!!!! Without this, tag brains are selected for non-tag cars!
            selected_brains = []

            if self.settings.mode == MODE_TRAINING:
                # print(f"SELECTING {text[round_]} BRAINS")
                # Select ARCHIVE PICKS best brains from archive
                # print(archive[round_].sorted_archive)
                # print(len(archive[round_].sorted_archive))
                # print(archive_picks[round_])
                if self.settings.use_archive:
                    first_picks = archive[round_].select_brains_with_weights(archive_picks[round_])
                else:
                    if round_ == tag:
                        first_picks = self.best_tag_brain_store
                    else:
                        first_picks = self.best_non_tag_brain_store

                # print(f"First picks: {first_picks}")

                if round_ == tag:
                    # print(f"Archive brain picks:")
                    for brain_ in first_picks:
                        # print(brain_)
                        pass

                # Clone the selection
                for brain in first_picks:
                    # If only 1 car: don't use the selected brain, only use the cloned brain
                    if cars[round_] > 1:
                        selected_brains.append(brain)

                    # print(f"Parent: {parent_brain}")
                    for _ in range(clones_per_archive_pick[round_]):
                        if round_ == tag:
                            # print(f"Cloning a brain...")
                            pass
                        selected_brains.append(brain.clone_and_evolve())
                    # print(f"Clone:  {brain_copy}")

                # print(f"Brains cloned")

                # Not enough brains for all cars? (Archive too small) - generate new brains until we have enough
                while len(selected_brains) < cars[round_]:
                    if round_ == tag:
                        # print(f"Generating extra brain from scratch...")
                        pass
                    selected_brains.append(random.choice(available_brains[round_])(number_of_inputs=self.number_of_sensors))

                # Too many brains? Remove extra
                selected_brains = selected_brains[:cars[round_]]

                # print(f"Selected brains: {selected_brains}")

            elif self.settings.mode == MODE_COMPETITION:
                if round_ == tag:
                    selected_brains.extend(self.COMPETITION_BRAIN_INSTANCES_TAG)
                else:
                    selected_brains.extend(self.COMPETITION_BRAIN_INSTANCES_NON_TAG)
            # print(f"Creating cars")
            # print(f"Level is {self.level.name}")

            # Inform Brains that a new round has started
            for brain_ in selected_brains:
                brain_.start_new_round()

            # Create Cars
            for brain_ in selected_brains:
                is_tag = round_ == tag
                # print(f"round_ == tag? {is_tag}")
                heading = randint(0, 359)
                if round_ == tag:
                    if isinstance(self.level, TagTrainingLevel):
                        heading = self.level.tag_start_heading
                else:
                    if isinstance(self.level, TagTrainingLevel):
                        heading = self.level.non_tag_start_heading

                self._place_new_car(brain=brain_, is_tag=is_tag, heading=heading,
                                    active_groups=(self.all_active_cars_group, group_active[round_]),
                                    inactive_group=group_inactive[round_])

        # Place obstacle cars
        if hasattr(self.level, 'obstacle_tag_cars') and self.level.obstacle_tag_cars is not None:
            for coordinate in self.level.obstacle_tag_cars:
                self._place_new_car(brain=ObstacleBrain(8), is_tag=True, heading=0,
                                    active_groups=(self.all_active_cars_group, self.obstacle_cars_group),
                                    inactive_group=self.inactive_cars_tag,
                                    position=coordinate,
                                    is_obstacle=True)

    def _place_new_car(self, brain: BaseBrain, is_tag: bool, heading: int,
                       active_groups: Tuple[Group, ...], inactive_group: Group,
                       position: Optional[Tuple[int, int]] = None,
                       is_obstacle: bool = False) -> None:

        if position is None:
            position = self.level.get_valid_starting_position(is_tag=is_tag,
                                                              placement_tries=self.CAR_PLACEMENT_TRIES,
                                                              car_size=(32, 32))

        new_car = TagCar(level=self.level,
                         brain=brain,
                         pos=position,
                         heading=heading,
                         active_groups=active_groups,
                         inactive_group=inactive_group,
                         sensors_group=self.sensors_group,
                         sensor_templates=self.sensor_templates,
                         brain_reward_values=self.settings.BRAIN_REWARD_VALUES,
                         fuel=self.settings.car_fuel,
                         max_speed=self.settings.car_max_speed,
                         min_speed=self.settings.car_min_speed,
                         turning_speed=self.settings.car_turning_speed,
                         acceleration=self.settings.car_acceleration,
                         braking=self.settings.car_braking,
                         friction=self.settings.car_friction,
                         is_the_tag=is_tag,
                         is_obstacle=is_obstacle)

        for placement_try in range(self.CAR_PLACEMENT_TRIES):
            # Check if new car crashes right on start

            # Collisions with other cars
            if self.settings.tag_cars_collide or not is_tag:
                # Tag cars collide: check with all cars
                collision = pygame.sprite.spritecollideany(new_car, self.all_active_cars_group,
                                                           collided=pygame.sprite.collide_mask)
                if collision is not None and collision is not new_car:
                    # Collision found - get a new position and continue
                    print(f"Car placement: collides with another car, retry...")
                    new_car.position = self.level.get_valid_starting_position(is_tag=is_tag,
                                                                              placement_tries=self.CAR_PLACEMENT_TRIES,
                                                                              car_size=(32, 32))
                    continue

            elif not self.settings.tag_cars_collide:
                # Tag cars don't collide with each other: only check
                if is_tag:
                    collision = pygame.sprite.spritecollideany(new_car, self.non_tag_cars_group,
                                                               collided=pygame.sprite.collide_mask)
                    if collision is not None:
                        print(f"Car placement: collides with another car, retry...")
                        new_car.position = self.level.get_valid_starting_position(is_tag=is_tag,
                                                                                  placement_tries=self.CAR_PLACEMENT_TRIES,
                                                                                  car_size=(32, 32))
                        continue

            # Collisions with walls
            collision_wall = pygame.sprite.collide_mask(new_car, self.level)
            if collision_wall is not None:
                # Collision found - get a new position and continue
                print(f"Car placement: collides with wall, retry...")
                new_car.position = self.level.get_valid_starting_position(is_tag=is_tag,
                                                                          placement_tries=self.CAR_PLACEMENT_TRIES,
                                                                          car_size=(32, 32))
                continue

    def run(self):
        self.running = True

        ##################
        # EVENT HANDLING #
        ##################

        while self.running:
            # VERY DEBUG
            # print(f"-------------NEW FRAME------------")
            mouse_click_position = None

            if self.new_round_just_started and self.settings.mode == MODE_COMPETITION:
                self.paused = True
                # print(f"ROUND WILL START WHEN YOU PRESS ENTER")

            if self.is_subprocess:
                # Command queue handling here
                pass
            else:
                for event in pygame.event.get():
                    if event.type == QUIT:
                        self.running = False
                    elif event.type == MOUSEBUTTONUP:
                        mouse_click_position = event.pos
                    elif event.type == KEYUP:
                        if event.key == K_ESCAPE:
                            self.running = False
                        elif event.key == K_SPACE:
                            # Space toggles fast forward
                            self.fast_forward = not self.fast_forward
                            print(f"Fast forward: {self.fast_forward}")
                        elif event.key == K_g:
                            # 'g' toggles graphics display
                            self.settings.draw_graphics = not self.settings.draw_graphics
                            print(f"DRAW GRAPHICS: {self.settings.draw_graphics}")
                        elif event.key == K_s:
                            # 's' toggles sensor display
                            self.settings.show_sensors = not self.settings.show_sensors
                            print(f"SHOW SENSORS: {self.settings.show_sensors}")
                        elif event.key == K_p:
                            self.paused = not self.paused
                            print(f"PAUSED: {self.paused}")
                        elif event.key == K_k:
                            print(f"KILLING ALL CARS")
                            for car in self.all_active_cars_group:
                                car.crash()
                        elif event.key == K_RSHIFT or event.key == K_LSHIFT:
                            self.slo_mo = not self.slo_mo
                            print(f"SLO-MO: {self.slo_mo}")
                        elif event.key == K_RETURN or event.key == K_KP_ENTER:
                            if self.settings.mode == MODE_COMPETITION and \
                                    self.paused and \
                                    self.new_round_just_started:
                                self.paused = False
                                self.new_round_just_started = False
                                print(f"ROUND STARTED!")

            ###############
            # CAR UPDATES #
            ###############

            if not self.paused:
                self.all_active_cars_group.update(self.level.rect, self.is_subprocess, self.settings.draw_graphics)

            ####################
            # COLLISION CHECKS #
            ####################

            # "Collisions" between mouse click and car
            # select car to show more info
            if self.settings.draw_graphics and mouse_click_position is not None:
                for car in self.all_active_cars_group:
                    if car.rect.collidepoint(mouse_click_position):
                        self.info_blurb_car = car
                        break

            if not self.paused:
                if len(self.non_tag_cars_group) > 0:
                    for car_ in self.non_tag_cars_group:
                        self.last_alive_non_tag_car = car_
                        break

                # Collisions with walls
                collisions = pygame.sprite.groupcollide(self.all_active_cars_group, LEVEL_GROUP,
                                                        dokilla=False, dokillb=False,
                                                        collided=pygame.sprite.collide_mask)
                for car in collisions:
                    # print(f"CRASH: collision with wall (Car {car.id})")
                    car.crash()
                # print(collisions)

                # Collisions with obstacle cars
                collisions = pygame.sprite.groupcollide(self.all_active_cars_group, self.obstacle_cars_group,
                                                        dokilla=False, dokillb=False,
                                                        collided=pygame.sprite.collide_mask)
                for car1, colliding_cars in collisions.items():
                    for car2 in colliding_cars:
                        if car1 is not car2:
                            # print(f"{car1} collided with obstacle car")
                            car1.crash()

                # Collisions between cars
                collisions = pygame.sprite.groupcollide(self.all_active_cars_group, self.all_active_cars_group,
                                                        dokilla=False, dokillb=False,
                                                        collided=pygame.sprite.collide_mask)
                for car1, colliding_cars in collisions.items():
                    for car2 in colliding_cars:
                        if car1 is not car2:
                            # print(f"Collision: {car1.brain}, {car2.brain}")

                            # Car 1 crashes if both are of the same type (tag/non-tag) OR
                            # Car 1 is non-tag and car 2 is tag
                            if (car1.is_the_tag == car2.is_the_tag) or (not car1.is_the_tag and car2.is_the_tag):
                                # print(f"CRASH: collision with another car (Car {car1.id})")
                                if not self.settings.tag_cars_collide and car1.is_the_tag:
                                    # Don't crash if both are tag cars and they don't collide with each other
                                    pass
                                else:
                                    car1.crash()

                            # If car 1 is the tag and car 2 isn't then reward catch points to car 1
                            elif car1.is_the_tag and not car2.is_the_tag:
                                car1.catch()  # updates brain score

                ##################################################
                # RANDOMLY KILL STATIONARY CARS IN TRAINING MODE #
                ##################################################

                if self.settings.mode == MODE_TRAINING:
                    random_number = random.random()
                    stationary_cars = []
                    if random_number < self.settings.stationary_tag_kill_probability:
                        for car_ in self.tag_cars_group:
                            if car_.is_stationary(frames=self.settings.stationary_frames_kill_treshold):
                                stationary_cars.append(car_)

                    if random_number < self.settings.stationary_non_tag_kill_probability:
                        for car_ in self.non_tag_cars_group:
                            if car_.is_stationary(frames=self.settings.stationary_frames_kill_treshold):
                                stationary_cars.append(car_)

                    if len(stationary_cars) > 0:
                        car_to_kill = random.choice(stationary_cars)
                        # print(f"CRASH: too slow: {car_to_kill.speed} (Car {car_to_kill.id})")
                        car_to_kill.crash()

                ###########################################################
                # SAVE SENSOR DATA TO FILE FOR FURTHER BRAIN TRAINING USE #
                ###########################################################

                if self.settings.save_sensor_data:
                    with open(self.TAG_SENSOR_DATA_FILENAME, 'a') as f:
                        for car in self.tag_cars_group:
                            # Write sensor values and decisions, comma separated
                            sensor_string = f"{','.join([str(i) for i in car.last_sensor_values])}"
                            command_string = f"{','.join([str(car.last_commands[c]) for c in (ACCELERATE, BRAKE, TURN_LEFT, TURN_RIGHT)])}"

                            f.write(f"{sensor_string},{command_string}\n")

                    with open(self.NON_TAG_SENSOR_DATA_FILENAME, 'a') as f:
                        for car in self.non_tag_cars_group:
                            # Write sensor values and decisions, comma separated
                            sensor_string = f"{','.join([str(i) for i in car.last_sensor_values])}"
                            command_string = f"{','.join([str(car.last_commands[c]) for c in (ACCELERATE, BRAKE, TURN_LEFT, TURN_RIGHT)])}"

                            f.write(f"{sensor_string},{command_string}\n")

                ##################
                # ROUND END HERE #
                ##################

                # No cars left? Add the best non-duplicate brain from inactive cars to archive
                if self.settings.non_tag_training_mode:
                    # End round when no non-tag cars left
                    round_end = (len(self.non_tag_cars_group) <= 0)
                elif hasattr(self.level, 'keep_spawning_non_tags') and self.level.keep_spawning_non_tags:
                    round_end = (len(self.tag_cars_group) <= 0)
                else:
                    # End round when no tag OR non-tag cars left
                    round_end = (len(self.tag_cars_group) <= 0 or len(self.non_tag_cars_group) <= 0)

                if hasattr(self.level, 'keep_spawning_non_tags') and \
                        self.level.keep_spawning_non_tags and \
                        len(self.non_tag_cars_group) == 0:
                    self._place_new_car(brain=self.non_tag_archive.best_brain().clone_and_evolve(),
                                        active_groups=(self.all_active_cars_group, self.non_tag_cars_group),
                                        inactive_group=self.inactive_cars_non_tag,
                                        heading=self.level.non_tag_start_heading,
                                        is_tag=False)

                if round_end:
                    print(f"End round")

                    tag_points_average = 0
                    non_tag_points_average = 0

                    if len(self.tag_cars_group) > 0 and len(self.non_tag_cars_group) > 0:
                        winner = None
                        print(f"IT'S A TIE ROUND!")
                    elif len(self.non_tag_cars_group) == 0:
                        print(f"TAG CARS WIN!")
                        winner = TAG
                    else:
                        print(f"NON-TAG CARS WIN!")
                        winner = NON_TAG

                    # print(f"Tag cars group: {self.tag_cars_group}")
                    # Add surviving cars to inactive cars - otherwise they're not included here!
                    for car_ in self.tag_cars_group:
                        if winner == TAG:
                            car_.survived_round()  # Awards points
                        if self.last_alive_non_tag_car is not None:
                            car_.award_points_for_near_non_tag(self.last_alive_non_tag_car.position)
                        car_.kill()  # Removes from active groups, adds to inactive group

                    for car_ in self.non_tag_cars_group:
                        if winner == NON_TAG:
                            car_.survived_round()  # Awards points
                        car_.kill()  # Removes from active groups, adds to inactive group

                    for car_ in self.obstacle_cars_group:
                        car_.kill()

                    tag_brains_sorted = []

                    pid_string = f"PID {self.pid} " if self.pid is not None else ""

                    if not self.settings.non_tag_training_mode:
                        tag_brains = [car.brain for car in self.inactive_cars_tag]
                        # print(f"tag_brains: {tag_brains}")
                        tag_brains_sorted = BaseBrain.sort_by_attribute(tag_brains, 'points_this_round')

                        # Gather aggregate data
                        tag_scores = [brain.points_this_round for brain in tag_brains]
                        tag_points_average = mean(tag_scores)

                        best_tag_brain_this_round = tag_brains_sorted[0]
                        # self.tag_archive.add_brain_if_big_enough_score(best_tag_brain_this_round)

                        print(f"-------------------------")
                        print(f"{pid_string}Best TAG brain for this round: {best_tag_brain_this_round}")
                        print(best_tag_brain_this_round.points_this_round_per_reason)
                        print(best_tag_brain_this_round.full_stats())
                        self.best_tag_brain_store = [best_tag_brain_this_round]
                    # print(f"All brains (sorted): {brains_sorted}")

                    non_tag_brains = [car.brain for car in self.inactive_cars_non_tag]
                    non_tag_brains_sorted = BaseBrain.sort_by_attribute(non_tag_brains, 'points_this_round')
                    best_non_tag_brain_this_round = non_tag_brains_sorted[0]
                    # print(f"inactive_cars_non_tag: {self.inactive_cars_non_tag}")
                    # self.non_tag_archive.add_brain_if_big_enough_score(best_non_tag_brain_this_round)

                    # Gather aggregate data
                    non_tag_scores = [brain.points_this_round for brain in non_tag_brains]
                    non_tag_points_average = mean(non_tag_scores)

                    print(f"-------------------------")
                    print(f"{pid_string}Best NON-TAG brain for this round: {best_non_tag_brain_this_round}")
                    print(f"POINTS THIS ROUND: {best_non_tag_brain_this_round.points_this_round}")
                    print(f"AVERAGE POINTS: {best_non_tag_brain_this_round.score}")
                    print(best_non_tag_brain_this_round.points_this_round_per_reason)
                    self.best_non_tag_brain_store = [best_non_tag_brain_this_round]

                    if self.settings.use_archive and self.settings.mode == MODE_TRAINING:
                        # Add the best brains to archive - minimum 1 car
                        surviving_tag_brains = max(1, int(self.settings.tag_cars * self.settings.percentage_cars_survive))
                        surviving_non_tag_brains = max(1, int(self.settings.non_tag_cars * self.settings.percentage_cars_survive))

                        # Add to archive if good enough average score

                        if not self.settings.non_tag_training_mode:
                            print(f"Adding tag brains to archive...")

                            for brain_ in tag_brains_sorted[:surviving_tag_brains]:
                                # Don't add human controlled brains to archive
                                if not isinstance(brain_, HumanControlledBrain):
                                    self.tag_archive.add_brain_if_big_enough_score(brain_)
                        print(f"Adding non-tag brains to archive...")
                        for brain_ in non_tag_brains_sorted[:surviving_non_tag_brains]:
                            self.non_tag_archive.add_brain_if_big_enough_score(brain_)

                        # Save archive after every round
                        self.tag_archive.save()
                        self.non_tag_archive.save()

                    if self.settings.mode == MODE_TRAINING:
                        # Save and display aggregate data
                        tag_points_string =     f"TAG brains average score    : {tag_points_average}"
                        non_tag_points_string = f"NON-TAG brains average score: {non_tag_points_average}"
                        print(tag_points_string)
                        print(non_tag_points_string)

                        with open(self.AGGREGATE_STATS_FILENAME, 'a') as f:
                            f.write(str(datetime.now()) + '\n')
                            f.write(tag_points_string + '\n')
                            f.write(non_tag_points_string + '\n')

                    print(f"ROUND FINISHED")

                    # Next level
                    try:
                        self.select_next_level()
                        self.create_cars_and_brains()
                    except IndexError:
                        print(f"ALL LEVELS FINISHED")
                        if FOREVER_TRAINING_MODE:
                            self._select_level(0)
                            self.create_cars_and_brains()
                        else:
                            self.running = False

            ##########################
            # DRAWING TO SCREEN HERE #
            ##########################

            if not self.is_subprocess and self.settings.draw_graphics:
                self.win.fill(Color('white'), self.game_area_rect)
                self.win.fill(Color('black'), self.info_area_rect)

                LEVEL_GROUP.draw(self.win)
                FINISH_LINE_GROUP.draw(self.win)

                # Sensors
                if self.settings.show_sensors:
                    LINE_SENSORS_GROUP.update(self.settings.show_sensors)  # gives sensors info that they need to calculate stuff
                    # This part works only on point sensors
                    for car in self.all_active_cars_group:
                        for sensor in car.sensors:
                            # Break if not PointSensor
                            if isinstance(sensor, PointSensor):
                                col = RED if sensor.value else YELLOW
                                pygame.draw.circle(pygame.display.get_surface(), col, intify_tuple(sensor.position), 5)
                            elif isinstance(sensor, LineSensor):
                                col = sensor.color
                                pygame.draw.line(pygame.display.get_surface(), col,
                                                 sensor.rect.center, car.rect.center, 1)

                    # LINE_SENSORS_GROUP.draw(self.win)

                # Draw all cars
                self.all_active_cars_group.draw(self.win)

                if SHOW_BEST_CAR:
                    # best_car = None

                    # ONLY SHOW BEST >TAG< CAR
                    for car in self.tag_cars_group:
                        if car.brain.points_this_round > self.highest_points_this_round:
                            self.highest_points_this_round = car.brain.points_this_round
                            self.best_car_this_round = car
                            # best_car = car
                            # self.best_car_last_known_pos = car.rect.center

                    if self.best_car_this_round is not None:
                        pygame.draw.circle(self.win, GREEN, self.best_car_this_round.rect.center, 24, 3)

                # Show / hide infoblurb
                # Use best car by default
                if self.info_blurb_car is None:
                    self.info_blurb_car = self.best_car_this_round

                if self.info_blurb_car is not None:
                    info_data = {
                        "ID": self.info_blurb_car.brain.id,
                        "This round": self.info_blurb_car.brain.points_this_round,
                        "Avg score": self.info_blurb_car.brain.score,
                        "Fuel": self.info_blurb_car.fuel,
                        }

                    for reason in SCORE_REASONS:
                        info_data[reason] = self.info_blurb_car.brain.points_this_round_per_reason[reason]

                    info_data['Actual speed'] = self.info_blurb_car.speed

                    sensor_values = self.info_blurb_car.last_sensor_values
                    # print(sensor_values)
                    # print(dir(sensor_values))

                    for field in sensor_values._fields:
                        info_data[field] = getattr(sensor_values, field)
                    # for i in range(self.number_of_sensors):
                    #     info_data[f's{i}'] = sensor_values[i]

                    if not self.new_round_just_started:
                        info_data['ACCELERATE'] = self.info_blurb_car.last_commands[ACCELERATE]
                        info_data['BRAKE'] = self.info_blurb_car.last_commands[BRAKE]
                        info_data['LEFT'] = self.info_blurb_car.last_commands[TURN_LEFT]
                        info_data['RIGHT'] = self.info_blurb_car.last_commands[TURN_RIGHT]
                    else:
                        info_data['ACCELERATE'] = 0
                        info_data['BRAKE'] = 0
                        info_data['LEFT'] = 0
                        info_data['RIGHT'] = 0

                    try:
                        info_data['Memory'] = self.info_blurb_car.brain._memory
                    except AttributeError:
                        info_data['Memory'] = -999

                    self.info_blurb.info_data = info_data

                    pygame.draw.circle(self.win, Color("yellow"), self.info_blurb_car.rect.center, 24, 3)
                    self.info_blurb.add(UI_GROUP)
                else:
                    self.info_blurb.remove(UI_GROUP)

                if SHOW_CAR_NUMBERS:
                    CAR_NUMBERS_GROUP.draw(self.win)

                UI_GROUP.draw(self.win)

                # print(f"PointSensor values: {car.get_sensor_values()}")

                pygame.display.flip()

                if not self.fast_forward:
                    self.clock.tick(FPS if not self.slo_mo else 1)

        # Here if quit
        self.save_and_quit()

    # def add_brains_to_archive(self, car_list):
    #     for car in car_list:
    #         self.archive.add_brain_if_big_enough_score(car.brain)

    def save_and_quit(self):
        self._save_settings()
        if self.settings.mode == MODE_TRAINING:
            if self.settings.use_archive:
                self.tag_archive.save()
                self.tag_archive.save_as_text()
                self.non_tag_archive.save()
                self.non_tag_archive.save_as_text()
            else:
                with open(self.settings.best_tag_brain_filename, 'wb') as f:
                    pickle.dump(self.best_tag_brain_store, f)
                with open(self.settings.best_non_tag_brain_filename, 'wb') as f:
                    pickle.dump(self.best_non_tag_brain_store, f)
        sys.exit(0)


if __name__ == '__main__':
    Program()



