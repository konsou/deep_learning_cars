import pygame
from typing import Tuple
from groups import UI_GROUP

TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT, CENTER = range(5)


class InfoBlurb(pygame.sprite.Sprite):
    def __init__(self, info_data: dict,
                 position_coordinates: Tuple[int, int] = (0, 0),
                 position_code: int = CENTER,
                 color=(255, 255, 255), bgcolor=(0, 0, 0),
                 font_size=24, group=UI_GROUP):
        pygame.sprite.Sprite.__init__(self, group)

        self._position = position_coordinates
        self._position_code = position_code  # TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT, CENTER
        self._color = color
        self._bgcolor = bgcolor
        self._font_size = font_size

        self.rect: pygame.Rect = pygame.Rect(0, 0, 0, 0)

        self._position_code_to_rect_attribute = {
            TOP_LEFT: self.rect.topleft,
            TOP_RIGHT: self.rect.topright,
            BOTTOM_LEFT: self.rect.bottomleft,
            BOTTOM_RIGHT: self.rect.bottomright,
        }

        self._info_data = info_data  # updated by method

        self._font_object = pygame.font.Font(None, self._font_size)

        self._image_height = font_size * len(info_data)
        self._label_width = 0
        self._number_of_rows = len(info_data)

        # Pre-render labels - these shouldn't change
        self.label_images = dict()
        # Value images - these are re-rendered when values change
        self.value_images = dict()

        value_image_width = 0

        for key, value in info_data.items():
            label_image = self._font_object.render(str(key), 1, self._color, self._bgcolor)
            label_width = label_image.get_width()
            if label_width > self._label_width:
                self._label_width = label_width
            self.label_images[key] = label_image

            value_image = self._font_object.render(str(value), 1, self._color, self._bgcolor)
            self.value_images[key] = value_image

        # print(self.label_images)
        # This updates the image
        self.info_data = info_data

        # This updates rect position
        self.position = position_coordinates

    def _set_correct_position_rect_attribute(self, new_value) -> None:
        if self._position_code == TOP_LEFT:
            self.rect.topleft = new_value
        elif self._position_code == TOP_RIGHT:
            self.rect.topright = new_value
        elif self._position_code == BOTTOM_LEFT:
            self.rect.bottomleft = new_value
        elif self._position_code == BOTTOM_RIGHT:
            self.rect.bottomright = new_value
        elif self._position_code == CENTER:
            self.rect.center = new_value
        else:
            raise ValueError(f"Incorrect position code")

    def _get_position(self) -> Tuple[int, int]:
        return self._position

    def _set_position(self, new_position) -> None:
        self._position = new_position
        self._set_correct_position_rect_attribute(new_position)

    def _get_left(self):
        return self.rect.left

    def _set_left(self, value):
        self.rect.left = value
        self._position = self.rect.center

    def _get_y(self):
        return self.rect.y

    def _set_y(self, value):
        self.rect.y = value
        self._position = self.rect.center

    def _get_info_data(self):
        return self._info_data

    def _set_info_data(self, new_data: dict):
        # print(new_data)

        # Stitch together a new image
        new_value_images = []
        value_width = 0
        for key, value in new_data.items():
            # Don't render if data hasn't changed
            if self.info_data[key] != value:
                value_image = self._font_object.render(str(round(value, 2)), 1, self._color, self._bgcolor)
                self.value_images[key] = value_image
            else:
                value_image = self.value_images[key]

            current_value_image_width = value_image.get_width()
            if current_value_image_width > value_width:
                value_width = current_value_image_width
            new_value_images.append(value_image)

        self.image = pygame.Surface((self._label_width + 5 + value_width, self._image_height))

        for row, (_, label_image) in enumerate(self.label_images.items()):
            self.image.blit(label_image, (0, row * self._font_size))

        for row, value_image in enumerate(new_value_images):
            self.image.blit(value_image, (self._label_width + 5, row * self._font_size))

        self._info_data = new_data

        self._update_rect()

    def _update_rect(self) -> None:
        self.rect = self.image.get_rect()
        self._set_correct_position_rect_attribute(self._position)

    position = property(_get_position, _set_position)
    left = property(_get_left, _set_left)
    y = property(_get_y, _set_y)
    info_data = property(_get_info_data, _set_info_data)

