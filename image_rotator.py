import pygame


def rotate_image_keep_center(original_image, angle):
    """rotate an image while keeping its center and size"""
    # Tämä copypastettu jostain netistä. Vaatii että kuva on neliö.
    assert original_image.get_width() == original_image.get_height(), \
        f"Can't rotate image - not square. {original_image}"
    orig_rect = original_image.get_rect()
    _image = pygame.transform.rotate(original_image, angle)
    rot_rect = orig_rect.copy()
    rot_rect.center = _image.get_rect().center
    _image = _image.subsurface(rot_rect).copy()
    return _image
