import pickle
import datetime
import os
from random import choices
from typing import Optional, Deque, Iterable, List
from collections import deque
from operator import attrgetter
from brains.brain_base import BaseBrain
from constants import *


class BrainArchive:
    def __init__(self, name: str = "", max_size: int = BRAIN_ARCHIVE_SIZE,
                 filename: str = BRAIN_ARCHIVE_FILENAME,
                 stats_dump_filename: str = STATS_DUMP_FILENAME) -> None:
        self.name = name
        self.max_size = max_size
        self.filename = filename
        self.stats_dump_filename = stats_dump_filename
        self._archive = deque(maxlen=max_size)
        self._sort_key_function = attrgetter("points")

    def __len__(self) -> int:
        return len(self._archive)

    def _get_archive(self) -> Deque[BaseBrain]:
        # No sorting by default
        # self._get_sorted_archive()
        return self._archive

    def _set_archive(self, value: Deque[BaseBrain]) -> None:
        self._archive = deque(value, maxlen=self.max_size)

    def _get_highest_id_in_use(self) -> int:
        highest_id = 0
        for brain in self.archive:
            if brain.id > highest_id:
                highest_id = brain.id
        return highest_id

    def change_max_size(self, new_max_size: int) -> None:
        self.max_size = new_max_size
        self._archive = deque(self._archive, maxlen=new_max_size)

    def worst_brain(self) -> Optional[BaseBrain]:
        """ Return the worst Brain (or None if the archive is empty) """
        sorted_archive = self._get_sorted_archive()
        try:
            return sorted_archive[-1]
        # Here if deque is empty
        except IndexError:
            return None

    def best_brain(self) -> Optional[BaseBrain]:
        """ Return the best Brain (or None if the archive is empty) """
        sorted_archive = self._get_sorted_archive()
        try:
            return sorted_archive[0]
        # Here if deque is empty
        except IndexError:
            return None

    def select_brains_with_weights(self, number_of_choices: int, weight_multiplier: float = 2) -> List[BaseBrain]:
        """Return a list of number_of_choices Brains. Selection is weighed by Brain score.
        For weight purposes, Brain scores are multiplied by weight_multiplier -
        bigger value leads to more probable selection of higher-scored Brains."""

        print(f"Selecting {number_of_choices} from {self.name} archive")

        if len(self.archive) <= 0:
            return []

        # Don't return more items than there are
        if number_of_choices >= len(self.archive):
            return list(self.archive)

        sorted_archive = list(self.sorted_archive)[:number_of_choices]  # deque can't be sliced, must be a list
        weights = []

        for brain_ in sorted_archive:
            # No negative weights. Negative weights crash.
            weight = brain_.points * weight_multiplier
            if weight < 0:
                weight = min(1.0, 1 / abs(weight))
            elif weight < 1:
                weight = 1

            weights.append(weight)

        # Avoid duplicates with this
        selected_brains = []
        for _ in range(number_of_choices):
            current_selection = choices(sorted_archive, weights=weights, k=1)[0]
            current_weight = weights[sorted_archive.index(current_selection)]
            weights.remove(current_weight)
            sorted_archive.remove(current_selection)
            selected_brains.append(current_selection)

        # print(f"Selected brains:")
        # for brain_ in selected_brains:
        #     print(brain_)

        return selected_brains

    def add_brain(self, brain: BaseBrain) -> bool:
        """Just add the given Brain to the left of the archive deque
        Only check for duplicates - no other checks
        The rightmost Brain is discarded
        Return True/False - whether the Brain was added"""
        if brain not in self.archive:
            worst_brain = self.worst_brain()
            if worst_brain is not None:
                worst_score = worst_brain.score
            else:
                worst_score = "None"
            print(f"Added {brain} to archive (worst was {worst_score})")
            self.archive.appendleft(brain)
            return True
        else:
            # print(f"NOT adding duplicate: {brain}")
            return False

    def _force_add_brain(self, brain: BaseBrain) -> None:
        """Force add the given Brain to left of archive - don't check for duplicates.
        USE WITH CAUTION! DUPLICATES ARE NOT NICE!"""
        self.archive.appendleft(brain)

    def add_brain_dont_remove_best(self, brain: BaseBrain) -> bool:
        """ Never remove the best Brain"""
        best_brain = self.best_brain()
        if best_brain is not None and self.archive[-1] == best_brain:
            print(f"Best brain {best_brain.id} about to get purged - re-add to archive")
            # Re-add the best brain if it is about to get purged
            self._force_add_brain(best_brain)

        return self.add_brain(brain)

    def add_all_brains_if_big_enough_score(self, brains: Iterable[BaseBrain]) -> int:
        """Add all given brains to the archive if their score is high enough.
        Return number of brains added."""
        number_of_brains_added = 0
        for brain in brains:
            if self.add_brain_if_big_enough_score(brain):
                number_of_brains_added += 1
        return number_of_brains_added

    def add_brain_if_big_enough_score(self, brain: BaseBrain) -> bool:
        """ Adds the given brain to archive IF:
        -there is enough space
        -if there isn't enough space, add the brain if it's score is better than the worst score in archive

        Return True/False, signaling if the brain was added or not
        """
        # print(f"Trying to add brain {brain.id} to archive...")

        if len(self) < self.max_size:
            # There's room left - just add
            return self.add_brain(brain)
        else:
            # No room left - check brain points
            # print(f"Worst brain in archive is {self.worst_brain()}")
            worst_brain_points = self.worst_brain().points

            # Points average AND last round points need to be good enough
            if brain.points > worst_brain_points and brain.points_this_round > worst_brain_points:
                return self.add_brain(brain)

        # print(f"Not adding brain {brain.id} to archive - too low score")
        return False

    def save(self) -> None:
        # split1, split2 = BRAIN_ARCHIVE_FILENAME.split(".")
        # filename = f"{split1}_{os.getpid()}.{split2}"
        filename = self.filename
        print(f"Save brain archive to {filename}")
        with open(filename, 'wb') as f:
            pickle.dump(self, f)

    def save_as_text(self) -> None:
        sorted_archive = self._get_sorted_archive()

        with open(self.stats_dump_filename, 'a') as f:
            f.write(f"ARCHIVE DUMP {os.getpid()} {datetime.datetime.now()}:\n")
            for brain in sorted_archive:
                f.write(repr(brain.full_stats()) + "\n")
                # print(brain.full_stats())
            try:
                last_line = \
                    f"----------------------------------------\n" \
                    f"BEST BRAIN FOR PID {os.getpid()}: {sorted_archive[0]}\n" \
                    f"----------------------------------------\n"
                f.write(last_line)
                print(last_line)
            except IndexError:
                # Archive empty
                f.write("Archive empty")
                print(f"Archive empty")

    def _get_sorted_archive(self) -> Deque[BaseBrain]:
        """ Returns a sorted-by-score copy of the archive deque """
        # print(f"Archive before sort: {self.archive}")
        return deque(sorted(self._archive, key=self._sort_key_function, reverse=True), maxlen=self.max_size)
        # print(f"Archive after sort: {self.archive}")

    archive = property(_get_archive, _set_archive)
    sorted_archive = property(_get_sorted_archive)
    highest_id_in_use = property(_get_highest_id_in_use)


def load_archive(filename: str = BRAIN_ARCHIVE_FILENAME) -> BrainArchive:
    print(f"Load brain archive from {filename}")
    with open(filename, 'rb') as f:
        archive_ = pickle.load(f)
    return archive_


if __name__ == '__main__':
    archive = load_archive(filename="pickled_brains/pickled_brains_tag.pkl")
    # print(archive.archive)

    print(f"ALL:")
    for brain in archive.sorted_archive:
        print(f"Brain {brain.id} - points {brain.points}")

    archive.best_brain().save_to_file('pickled_brains/')

    # print(f"SELECTION:")
    # for brain in archive.select_brains_with_weights(5):
    #     print(f"Brain {brain.id} - points {brain.points}")

    # print(archive.archive)
    # archive.change_max_size(5)
    # print(archive.archive)
