import pygame
from typing import NamedTuple, Tuple, List, Optional
from abc import ABC, abstractmethod
from math import sqrt
from random import gauss, random, randint
from pygame.math import Vector2
from pygame.sprite import Group
from utility import intify_tuple
import car
from groups import *
from colors import *
from constants import *


"""ALL SENSORS: FIRST TWO INIT ARGUMENTS MUST BE car AND level!!!"""


class BaseSensor(ABC, pygame.sprite.Sprite):
    def __init__(self, car, level: 'BaseLevel', group: Group,
                 label: str,
                 noise_percentage: float = .1) -> None:
        pygame.sprite.Sprite.__init__(self, group)
        self.car = car
        self.level = level
        self.group = group
        self.label = label

        # SENSOR NOISE
        self.noise_percentage = noise_percentage
        # self.noise_sigma = noise_sigma
        self._lowest_sensor_readings: Optional[List[float]] = None
        self._highest_sensor_readings: Optional[List[float]] = None
        self._lowest_highest_reading_difference: Optional[List[float]] = None
        self._lowest_highest_reading_average: Optional[List[float]] = None

    def __repr__(self) -> str:
        return f"<{self.__class__.__name__}>"

    def add_noise(self, readings: Tuple[float, ...]) -> Tuple[float, ...]:
        """Calculate sensor noise and add it to given sensor values"""
        self.record_sensor_readings(readings)  # Updates sensor avg readings

        # TODO: THIS KINDA WORKS BUT I DON'T UNDERSTAND IT

        output = []
        for reading, mean, reading_difference in zip(readings, self._lowest_highest_reading_average,
                                                     self._lowest_highest_reading_difference):
            variance = sqrt(reading_difference / 2) * self.noise_percentage
            # noise = mean - gauss(mean, variance)
            noise = gauss(0, variance)
            # print(f"noise: {noise}")
            output.append(reading + noise)
        return tuple(output)

    def record_sensor_readings(self, readings: Tuple[float, ...]) -> None:
        """Record highest and lowest sensor readings for use in generating noise"""
        # print("----------------")
        # print(f"In record_sensor_readings - \n lowest: {self._lowest_sensor_readings}, "
        #       f"\nhighest: {self._highest_sensor_readings}")
        if self._lowest_sensor_readings is None:
            self._lowest_sensor_readings = list(readings)

        if self._highest_sensor_readings is None:
            self._highest_sensor_readings = list(readings)

        self._lowest_highest_reading_average = []
        self._lowest_highest_reading_difference = []

        for (index, reading), lowest, highest in zip(enumerate(readings),
                                                     self._lowest_sensor_readings,
                                                     self._highest_sensor_readings):
            # print(f"index: {index}, reading: {reading}, lowest: {lowest}, highest: {highest}")
            if reading < lowest:
                # print(f"{reading} is LOWER THAN {self._lowest_sensor_readings[index]} - storing...")
                self._lowest_sensor_readings[index] = reading
                lowest = reading
            if reading > highest:
                # print(f"{reading} is HIGHER THAN {self._highest_sensor_readings[index]} - storing...")
                self._highest_sensor_readings[index] = reading
                highest = reading

            self._lowest_highest_reading_difference.append(highest - lowest)
            self._lowest_highest_reading_average.append((highest + lowest) / 2)

        # print(f"In record_sensor_readings - \n lowest: {self._lowest_sensor_readings}, "
        #       f"\nhighest: {self._highest_sensor_readings}"
        #       f"\n    avg: {self._lowest_highest_reading_average}"
        #       f"\n   diff: {self._lowest_highest_reading_difference}")

    @property
    @abstractmethod
    def value(self) -> Tuple[float, ...]:
        """OVERRIDE THIS"""
        pass

    @property
    @abstractmethod
    def raw_value(self) -> Tuple[float, ...]:
        """OVERRIDE THIS"""
        pass


class FakeSensor(BaseSensor):
    """Fake Sensor for testing. Return random values. Sensors ALWAYS need a Car - use FakeCar for testing."""
    def __init__(self, car: 'BaseCar', level: 'BaseLevel', group: Group) -> None:
        BaseSensor.__init__(self, car=car, level=level, group=group, label="Fake")

    @property
    def raw_value(self) -> Tuple[float, ...]:
        sensor_values = [random.randint(1, 100) for i in range(2)]
        more_sensor_values = [random.random() for i in range(2)]
        sensor_values.extend(more_sensor_values)
        return tuple(sensor_values)

    @property
    def value(self) -> Tuple[float, ...]:
        # self.record_sensor_readings(sensor_values)
        # print(f"Sensor values before noise: {sensor_values}")
        return self.add_noise(self.raw_value)


class FakeSensorTemplate(NamedTuple):
    label: str
    sensor_type = FakeSensor
    number_of_outputs = 1


class SpeedSensor(BaseSensor):
    """Gives the speed of the car"""
    def __init__(self, car, level: 'BaseLevel', group: Group, label: str) -> None:
        BaseSensor.__init__(self, car, level, group, label=label)

    @property
    def raw_value(self) -> Tuple[float, ...]:
        return self.car.speed,

    @property
    def value(self) -> Tuple[float, ...]:
        return self.add_noise(self.raw_value)


class SpeedSensorTemplate(NamedTuple):
    label: str
    sensor_type = SpeedSensor
    number_of_outputs = 1


class CarRadarSensor(BaseSensor):
    def __init__(self, car, level: 'BaseLevel', group: Group,
                 label: str,
                 scan_group: pygame.sprite.Group) -> None:
        BaseSensor.__init__(self, car, level, group, label=label)
        self.scan_group = scan_group

    @property
    def raw_value(self) -> Tuple[float, ...]:
        """Return (distance, angle) to nearest car in scan group
        NO NOISE!
        ANGLE IS RELATIVE TO CAR HEADING!
        0 = straight ahead
        1...180   = n degrees counter-clockwise (Pygame style)
        -1...-179 = n degrees clockwise (Pygame style)"""
        lowest_distance_squared = 99999999999
        nearest_car = None

        for car in self.scan_group:
            if car is not self.car:
                distance_squared = self.car.position.distance_squared_to(car.position)
                if distance_squared < lowest_distance_squared:
                    lowest_distance_squared = distance_squared
                    nearest_car = car

        if nearest_car is not None:
            distance, angle = intify_tuple((nearest_car.position - self.car.position).as_polar())
            pygame_angle = (270 - angle) % 360
            angle_diff = pygame_angle - self.car.heading
            while angle_diff > 180:
                angle_diff -= 360
            while angle_diff < -180:
                angle_diff += 360
            return distance, angle_diff
        else:
            return 999, 0

    def _get_value(self) -> Tuple[float, ...]:
        """Return (distance, angle) to nearest car in scan group
        WITH NOISE ADDED!
        ANGLE IS RELATIVE TO CAR HEADING!
        0 = straight ahead
        1...180   = n degrees counter-clockwise (Pygame style)
        -1...-179 = n degrees clockwise (Pygame style)"""
        return self.add_noise(self.raw_value)

    value = property(_get_value)


class CarRadarSensorTemplate(NamedTuple):
    label: str
    scan_group: pygame.sprite.Group
    sensor_type = CarRadarSensor
    number_of_outputs = 2


class ZeroReturningCarRadarSensor(CarRadarSensor):
    @property
    def raw_value(self) -> Tuple[float, ...]:
        return 0, 0

    @property
    def value(self) -> Tuple[float, ...]:
        return 0, 0


class ZeroReturningCarRadarSensorTemplate(NamedTuple):
    label: str
    scan_group: pygame.sprite.Group
    sensor_type = ZeroReturningCarRadarSensor
    number_of_outputs = 2


class PointSensor(BaseSensor):
    def __init__(self, car, level: 'BaseLevel', group: Group,
                 label: str,
                 angle: int, distance: int):
        BaseSensor.__init__(self, car, level, group, label=label)
        self.distance = distance
        self.angle = angle
        self._value = None
        self._position = Vector2()

    @property
    def raw_value(self) -> Tuple[float, ...]:
        try:
            color_value = self.level.get_at((int(self.position[0]), int(self.position[1])))
            # print(color_value)
        except IndexError:
            # Given position is out of bounds - treat as wall
            return self.add_noise((1,))
        # Simplify things: return only the alpha value
        # Simplify things even more: only return True/False
        # True: WALL - False: NO WALL
        return float(bool(color_value[3])),

    @property
    def value(self) -> Tuple[float, ...]:
        return self.add_noise(self.raw_value)

    def _get_position(self):
        self._position.from_polar((self.distance, 270 - self.car.heading + self.angle))
        return self.car.position + self._position

    position = property(_get_position)


class PointSensorTemplate(NamedTuple):
    label: str
    angle: int
    distance: int
    sensor_type = PointSensor
    number_of_outputs = 1


class LineSensor(BaseSensor):
    """Senses walls only"""
    def __init__(self, car, level: 'BaseLevel', group: Group,
                 label: str,
                 angle: int, distance: int,
                 sensor_resolution: int,
                 show_sensors: bool = False):
        BaseSensor.__init__(self, car, level, group, label=label)
        self.angle = angle
        self.distance = distance
        self.sensor_resolution = sensor_resolution
        self.show_sensors = show_sensors

        # if SHOW_SENSORS:
        self.images = {
            GREEN: pygame.Surface((5, 5)).convert_alpha(),
            YELLOW: pygame.Surface((5, 5)).convert_alpha(),
            RED: pygame.Surface((5, 5)).convert_alpha(),
        }
        for color, surface in self.images.items():
            surface.fill(TRANSPARENCY)
            pygame.draw.circle(surface, color, (2, 2), 3)

        self.image = self.images[GREEN]
        self.color = GREEN
        self.rect = self.image.get_rect()
        self.rect.center = self.car.rect.center

    def update(self, show_sensors=False):
        self.show_sensors = show_sensors

    @property
    def raw_value(self) -> Tuple[float, ...]:
        step_vector = Vector2()
        step_vector.from_polar((self.sensor_resolution, 270 - self.car.heading + self.angle))

        # Create a copy of car position - otherwise messes up car position
        sensor_point = Vector2(self.car.position)

        for i in range(self.sensor_resolution, self.distance, self.sensor_resolution):
            sensor_point += step_vector
            value = self.level.get_at(intify_tuple(sensor_point))
            # print(f"{i} - Sensor value at {sensor_point}: {value}")
            if value[3] != 0:
                ret_val = i
                if self.show_sensors:
                    # self.rect.center = sensor_point
                    sensor_distance_ratio = i / self.distance
                    if sensor_distance_ratio < 0.33:
                        self.color = RED
                    elif sensor_distance_ratio < 0.66:
                        self.color = YELLOW
                    else:
                        self.color = GREEN
                break
        else:
            # Here if didn't break - no obstacles
            ret_val = self.distance
            if self.show_sensors:
                # sensor_position = Vector2()
                # sensor_position.from_polar((self.distance, 270 - self.car.heading + self.angle))

                self.color = GREEN

        # ret_val = int(self.car.position.distance_to(sensor_point))
        # print(f"Sensor: {ret_val}")
        if self.show_sensors:
            self.rect.center = sensor_point
            self.image = self.images[self.color]

        return ret_val,

    @property
    def value(self) -> Tuple[float, ...]:
        return self.add_noise(self.raw_value)


class LineSensorTemplate(NamedTuple):
    label: str
    angle: int
    distance: int
    sensor_resolution: int
    show_sensors: bool
    sensor_type = LineSensor
    number_of_outputs = 1


class TagLineSensor(LineSensor):
    """Measures distance to walls OR other cars - also return data on obstacle type"""

    NOTHING, WALL, NON_TAG_CAR, TAG_CAR = range(4)  # sensor tuple 2nd return value explanations

    def __init__(self, car, level: 'BaseLevel', group: Group,
                 label: str,
                 angle: int, distance: int,
                 sensor_resolution: int,
                 show_sensors: bool,
                 tag_cars_group: Group,
                 non_tag_cars_group: Group,
                 ):
        LineSensor.__init__(self, car, level, group, label=label,
                            angle=angle, distance=distance,
                            sensor_resolution=sensor_resolution,
                            show_sensors=show_sensors)
        self.tag_cars_group = tag_cars_group
        self.non_tag_cars_group = non_tag_cars_group

    def update(self, show_sensors=False):
        self.show_sensors = show_sensors

    @property
    def raw_value(self) -> Tuple[float, ...]:
        step_vector = Vector2()
        step_vector.from_polar((self.sensor_resolution, 270 - self.car.heading + self.angle))

        # Create a copy of car position - otherwise messes up car position
        sensor_point = Vector2(self.car.position)

        ret_val = None

        for i in range(self.sensor_resolution, self.distance, self.sensor_resolution):
            sensor_point += step_vector
            # print(f"SENSOR AT {sensor_point}:")
            # pygame.draw.circle(pygame.display.get_surface(), YELLOW, intify_tuple(sensor_point), 5)
            # pygame.display.flip()

            # Collision with wall?
            value = self.level.get_at(intify_tuple(sensor_point))
            # print(f"{i} - Sensor value at {sensor_point}: {value}")
            if value[3] != 0:
                ret_val = i, TagLineSensor.WALL
                # print(f"COLLISION WITH WALL: {ret_val}")

            # Collision with a tag car?
            if ret_val is None:
                for car in self.tag_cars_group:
                    if car is not self.car:
                        if car.rect.collidepoint(sensor_point):
                            ret_val = i, TagLineSensor.TAG_CAR
                            # print(f"COLLISION WITH TAG CAR: {ret_val}")

            # Collision with a non-tag car?
            if ret_val is None:
                for car in self.non_tag_cars_group:
                    if car is not self.car:
                        if car.rect.collidepoint(sensor_point):
                            ret_val = i, TagLineSensor.NON_TAG_CAR
                            # print(f"COLLISION WITH NON-TAG CAR: {ret_val}")

            if ret_val is not None:
                if self.show_sensors:
                    sensor_distance_ratio = i / self.distance
                    if sensor_distance_ratio < 0.33:
                        self.color = RED
                    elif sensor_distance_ratio < 0.66:
                        self.color = YELLOW
                    else:
                        self.color = GREEN
                break
        else:
            # Here if didn't break - no obstacles
            ret_val = self.distance, TagLineSensor.NOTHING
            if self.show_sensors:
                self.color = GREEN

        if self.show_sensors:
            self.rect.center = sensor_point
            self.image = self.images[self.color]

        return ret_val

    @property
    def value(self) -> Tuple[float, ...]:
        """Return tuple: (distance, obstacle type)"""

        return self.add_noise(self.raw_value)


class TagLineSensorTemplate(NamedTuple):
    label: str
    angle: int
    distance: int
    sensor_resolution: int
    show_sensors: bool
    tag_cars_group: Group
    non_tag_cars_group: Group
    sensor_type = TagLineSensor
    number_of_outputs = 2


if __name__ == '__main__':
    pygame.init()
    pygame.display.set_mode((1, 1))
    car = car.FakeCar()
    for _ in range(10):
        print(car.get_sensor_values())
