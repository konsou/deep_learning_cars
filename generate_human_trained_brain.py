from constants import *
from numpy import loadtxt
from keras.models import Sequential, Model
from keras.layers import Dense, Input
from brains.brain_human_trained import HumanTrainedBrain
import pickle


def create_and_train_model(sensor_data_file: str, output_model_filename: str) -> None:
    """return model filename"""
    # dataset = loadtxt('pima-indians-diabetes.data.csv', delimiter=',')
    dataset = loadtxt(sensor_data_file, delimiter=',')
    input_data = dataset[:, 0:8]
    n_input = 8
    user_outputs = dataset[:, 8:]

    model = Sequential()
    model.add(Dense(16, input_dim=n_input, activation='relu'))
    model.add(Dense(8, activation='relu'))
    model.add(Dense(4, activation='sigmoid'))
    # model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])

    model.fit(input_data, user_outputs, epochs=200, batch_size=10)

    print(f"Saving model as {output_model_filename}...")
    with open(output_model_filename, 'wb') as f:
        pickle.dump(model, f)

    loss, accuracy = model.evaluate(input_data, user_outputs)
    print(f"Accuracy: {round(accuracy * 100, 2)} %")


if __name__ == '__main__':
    sensor_data_filename = 'sensor_data/non_tag_sensor_data.csv'
    model_filename = 'human_trained_model.pkl'
    create_and_train_model(sensor_data_filename, model_filename)
    trained_brain = HumanTrainedBrain(number_of_inputs=8, model_file=model_filename)
    trained_brain.save_to_file('pickled_brains/')
