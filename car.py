import pygame
from pygame import Rect
from pygame.sprite import Group, Sprite
from pygame.math import Vector2
from typing import Iterable, Deque, Tuple, Optional, Collection, NamedTuple, List
from collections import deque
from abc import ABC, abstractmethod
from copy import copy
from image_rotator import rotate_image_keep_center
from angle_tools import angle_difference
from brains.brain_base import BrainInputTuple, RaceBrainInputTuple
from brains.brain_konso_human_controlled import HumanControlledBrain
from brains.brain_dummy import DummyBrainStationary
from sensor import BaseSensor, FakeSensorTemplate
from level import BaseLevel, FakeLevel
from text import Text
from groups import *
from constants import *
from colors import *


class BaseCar(ABC, Sprite):
    """ Car controls most of the stats of Brains """
    def __init__(self, level, brain, pos, heading: int,
                 active_groups: Tuple[Group, ...],
                 inactive_group: Group,
                 sensors_group: Group,
                 sensor_templates: Collection[NamedTuple],
                 brain_reward_values: dict,
                 fuel: float,
                 max_speed: float,
                 min_speed: float,
                 turning_speed: float,
                 acceleration: float,
                 braking: float,
                 friction: float,
                 ):

        pygame.sprite.Sprite.__init__(self, active_groups)

        self.level = level
        self.brain = brain

        # Sprite is in active groups when it's alive
        self.active_groups: Tuple[Group, ...] = active_groups
        # Moved to inactive group when it's crashed
        self.inactive_group: Group = inactive_group
        # Group for this car's sensors
        self.sensors_group: Group = sensors_group

        self.brain_reward_values = copy(brain_reward_values)

        # This made in main
        # self.brain.rounds_started += 1

        self._position: Vector2 = Vector2(pos)
        self._velocity: Vector2 = Vector2(0, 0)

        self._heading = heading  # degrees - pygame angle
        ########################################
        # PYGAME ANGLES:   VECTOR2 ANGLES:     #
        #                                      #
        #      0                 270           #
        #   90   270          180    0         #
        #     180                 90           #
        ########################################

        self._speed: float = 0
        self._acceleration: float = 0
        self._braking: float = 0
        self._fuel: float = fuel

        self.last_sensor_values: BrainInputTuple = BrainInputTuple(0, 0, 0, 0, 0, 0, 0, 0)
        self.last_commands: dict = dict()

        self.max_speed = max_speed
        self.min_speed = min_speed
        self.turning_speed = turning_speed
        self.acceleration_value = acceleration
        self.braking_value = braking
        self.friction: float = friction

        self.image = None
        self.original_image = pygame.image.load('images/car_32.png').convert_alpha()
        self.mask = None
        self.rotate_image()

        self.rect = self.image.get_rect()

        self.rect.center = pos

        # Init sensors
        self._sensors = []
        for sensor_template in sensor_templates:
            self.add_sensor(sensor_template, sensors_group)

        self._command_handlers = {
            ACCELERATE: self.accelerate,
            BRAKE: self.brake,
            TURN_LEFT: self.turn_left,
            TURN_RIGHT: self.turn_right,
        }

    def __repr__(self) -> str:
        return f"<{self.__class__.__name__} with brain {self.brain}>"

    def _get_position(self) -> Vector2:
        return self._position

    def _set_position(self, pos):
        self._position = pygame.math.Vector2(pos)
        self.rect.center = pos

    def get_position_as_int(self) -> Tuple[int, int]:
        return int(self._position[0]), int(self._position[1])

    def _get_velocity(self) -> Vector2:
        return self._velocity

    def _set_velocity(self, value):
        self._velocity = Vector2(value)

    def _is_bound(self) -> bool:
        return self._is_bound

    def _set_bound(self, bound: bool):
        self._is_bound = bound

    def _get_heading(self) -> float:
        return self._heading

    def _set_heading(self, new_heading: float):
        self._heading = new_heading % 360

    def _get_speed(self) -> float:
        return self._speed

    def _set_speed(self, new_speed: float):
        self._speed = new_speed

    def _get_acceleration(self) -> float:
        return self._acceleration

    def _set_acceleration(self, value: float):
        self._acceleration = value

    def _get_braking(self) -> float:
        return self._braking

    def _set_braking(self, value: float):
        self._braking = value

    def _get_sensors(self) -> List[BaseSensor]:
        return self._sensors

    def _get_fuel(self) -> float:
        return self._fuel

    def _get_id(self) -> int:
        """Cars don't have ID's, Brains do"""
        return self.brain.id

    def rotate_image(self):
        self.image = rotate_image_keep_center(self.original_image, self.heading)
        self.mask = pygame.mask.from_surface(self.image)

    def handle_command(self, command):
        self._command_handlers[command]()

    def add_sensor(self, sensor_template: NamedTuple, sensor_group: Group):
        sensor_constructor = sensor_template.sensor_type
        sensor = sensor_constructor(self, self.level, sensor_group, *sensor_template)
        self._sensors.append(sensor)

    def get_sensor_values(self) -> List[float]:
        ret_val = []
        for sensor in self._sensors:
            # print(f"got sensor value: {sensor.value}")
            sensor_value = sensor.value

            # Each Sensor can have multiple values
            ret_val.extend(sensor_value)
        # print(f"Sensor values: {ret_val}")
        return ret_val

    def get_raw_sensor_values(self) -> List[float]:
        """Get sensor values without noise"""
        ret_val = []
        for sensor in self._sensors:
            # print(f"got sensor value: {sensor.value}")
            sensor_value = sensor.raw_value

            # Each Sensor can have multiple values
            ret_val.extend(sensor_value)
        # print(f"Sensor values: {ret_val}")
        return ret_val

    def accelerate(self, rate: float):
        """Rate should be between 0 and 1"""
        # print("Accelerating")
        rate = min(rate, 1)
        rate = max(rate, 0)
        self.speed += self.acceleration_value * rate

    def brake(self, rate: float):
        """Rate should be between 0 and 1"""
        # print("Braking")
        rate = min(rate, 1)
        rate = max(rate, 0)
        self.speed -= self.braking_value * rate
        # self.speed = max(self.min_speed, self.speed)

    def turn_left(self, rate: float):
        """Rate should be between 0 and 1"""
        # print("Turning left")
        rate = min(rate, 1)
        rate = max(rate, 0)
        self.heading += self.turning_speed * rate

    def turn_right(self, rate: float):
        """Rate should be between 0 and 1"""
        # print("Turning right")
        rate = min(rate, 1)
        rate = max(rate, 0)
        self.heading -= self.turning_speed * rate

    def update(self, game_area_rect=None, is_subprocess=False, draw_graphics=True):
        sensor_values = self.get_sensor_values()

        accelerate, brake, turn_left, turn_right = self.brain.process_sensor_readings(sensor_values)

        self.last_sensor_values = BrainInputTuple(*sensor_values)
        self.last_commands = {
            ACCELERATE: accelerate,
            BRAKE: brake,
            TURN_LEFT: turn_left,
            TURN_RIGHT: turn_right,
        }

        # print(f"Commands from brain {self.brain.id}: {accelerate}, {brake}, {turn_left}, {turn_right}")
        if accelerate:
            self.accelerate(accelerate)
        if brake:
            self.brake(brake)
        if turn_left:
            self.turn_left(turn_left)
        if turn_right:
            self.turn_right(turn_right)

        # Apply friction
        self.speed = (1 - self.friction) * self.speed

        # Limit speed - max/min values
        self.speed = min(self.max_speed, self.speed)
        self.speed = max(self.min_speed, self.speed)

        # Stats
        self.brain.distance_traveled += self.speed
        self.brain.frames_alive += 1
        # self.brain.add_points(self.speed * POINTS_PER_DISTANCE_TRAVELED)
        # print(f"---\nCAR IS TAG: {self.is_the_tag}")
        self.brain.add_points(self.brain_reward_values['POINTS_PER_LIFETIME_TICK'],
                              reason=REASON_LIFETIME)

        # Fuel

        # FEATURE FREEZE - DON'T CHANGE!
        # Acceleration consumes fuel
        self._fuel -= self.acceleration_value * accelerate
        # Turning also consumes fuel - but less
        self._fuel -= max(turn_left, turn_right) * 0.1
        # Just keeping the motor running consumes a little fuel
        self._fuel -= 0.1

        if self._fuel <= 0:
            self.crash()

        # print(f"Speed: {self.speed}, heading: {self.heading}")
        velocity = pygame.math.Vector2()
        velocity.from_polar((self.speed, 270 - self.heading))  # Vector2 -> pygame angle direction fix
        # print(f"Velocity from polar: {velocity}, {velocity.as_polar()}")
        self.position += velocity

        # Crash if outside game area
        if self.rect.left < game_area_rect.left or self.rect.right > game_area_rect.right or \
                self.rect.top < game_area_rect.top or self.rect.bottom > game_area_rect.bottom:
            # print(f"Brain {self.brain.id} outside game area - crash")
            self.crash()

        if not is_subprocess and draw_graphics:
            self.update_graphics()

        # print(self)

    def update_graphics(self):
        self.rotate_image()

    def crash(self):
        """Kills the sprite, awards crash penalty"""
        # Remove from active groups, add to inactive group
        self.kill()

        self.brain.crashes += 1
        self.brain.add_points(self.brain_reward_values['POINTS_PER_CRASH'],
                              reason=REASON_CRASH)
        # print(f"CRASHED! {self} - {self.brain.points_this_round_per_reason}")

    def kill(self):
        """Kill label text and sensor as well
        Add Sprite to inactive group"""
        for sensor in self.sensors:
            sensor.kill()
        Sprite.kill(self)

        # Add to inactive group
        self.add(self.inactive_group)

    position = property(_get_position, _set_position)
    velocity = property(_get_velocity, _set_velocity)
    is_bound = property(_is_bound, _set_bound)
    heading = property(_get_heading, _set_heading)
    speed = property(_get_speed, _set_speed)
    acceleration = property(_get_acceleration, _set_acceleration)
    braking = property(_get_braking, _set_braking)
    sensors = property(_get_sensors)
    fuel = property(_get_fuel)
    id = property(_get_id)


class FakeCar(BaseCar):
    """Fake car for testing purposes"""
    def __init__(self):
        BaseCar.__init__(self, level=FakeLevel(), brain=DummyBrainStationary(1), pos=Vector2((0, 0)), heading=0,
                         active_groups=(Group(),),
                         inactive_group=Group(),
                         sensors_group=Group(),
                         sensor_templates=[FakeSensorTemplate()],
                         brain_reward_values={},
                         fuel=100,
                         max_speed=1,
                         min_speed=0,
                         turning_speed=1,
                         acceleration=1,
                         braking=1,
                         friction=0,
                         )


class TagCar(BaseCar):
    """ Car controls most of the stats of Brains """
    def __init__(self, level, brain, pos, heading: int,
                 active_groups: Tuple[Group, ...],
                 inactive_group: Group,
                 sensors_group: Group,
                 sensor_templates: Collection[NamedTuple],
                 brain_reward_values: dict,
                 fuel: float,
                 max_speed: float,
                 min_speed: float,
                 turning_speed: float,
                 acceleration: float,
                 braking: float,
                 friction: float,
                 is_the_tag: bool = False,
                 is_obstacle: bool = False):

        # print(f"TagCar init: is_the_tag: {is_the_tag}, pos: {pos}")

        BaseCar.__init__(self, level, brain, pos, heading,
                         active_groups=active_groups,
                         inactive_group=inactive_group,
                         sensors_group=sensors_group,
                         sensor_templates=sensor_templates,
                         brain_reward_values=brain_reward_values,
                         fuel=fuel,
                         max_speed=max_speed,
                         min_speed=min_speed,
                         turning_speed=turning_speed,
                         acceleration=acceleration,
                         braking=braking,
                         friction=friction)

        self._frames_stationary: int = 0

        # TAG CARS GET LESS POINTS FOR JUST STAYING ALIVE
        if is_the_tag:
            self.brain_reward_values['POINTS_PER_LIFETIME_TICK'] = self.brain_reward_values['POINTS_PER_LIFETIME_TICK_TAG']

        # if not is_the_tag and isinstance(brain, HumanControlledBrain):
        #     raise ValueError(f"HumanControlledBrain given to non-tag car: {brain}")

        self.is_the_tag: bool = is_the_tag

        # Obstacle: stationary, doesn't crash
        self.is_obstacle: bool = is_obstacle

        if self.is_the_tag:
            # Red car
            self.original_image = pygame.image.load('images/car_32.png').convert_alpha()
        else:
            self.original_image = pygame.image.load('images/car_32_green.png').convert_alpha()
        self.rotate_image()

        # Sounds
        self.tag_catch_sound: Optional[pygame.mixer.Sound] = None
        # self.tag_catch_sound: Optional[pygame.mixer.Sound] = pygame.mixer.Sound('sounds/auto-tursa.ogg')

    def catch(self):
        """Award points for catching a non-tag car"""
        if self.tag_catch_sound is not None:
            self.tag_catch_sound.play()
        self.brain.add_points(self.brain_reward_values['POINTS_PER_TAG_CATCH'],
                              reason=REASON_TAG_CATCH)

    def award_points_for_near_non_tag(self, non_tag_position: Optional[Vector2] = None) -> None:
        if self.is_the_tag:
            if non_tag_position is None:
                # Use sensor readings if non_tag_position is not given
                non_tag_distance = BrainInputTuple(*self.get_raw_sensor_values()).radar_non_tag_distance
            else:
                non_tag_distance = non_tag_position.distance_to(self.position)

            points = max(0,
                         (self.brain_reward_values['POINTS_PER_TAG_CATCH'] / 2) -
                         non_tag_distance *
                         self.brain_reward_values['POINTS_DEDUCT_PER_DISTANCE_TO_NON_TAG']
                         )
            # print(f"Car {self} got {points} points for being near a non-tag (dist: {non_tag_distance})")
            self.brain.add_points(points, reason=REASON_NEAR_NON_TAG_CAR)

    def crash(self) -> None:
        """Award points if near non-tag"""
        if not self.is_obstacle:
            self.award_points_for_near_non_tag()
            BaseCar.crash(self)

    def survived_round(self) -> None:
        """Award points for staying alive for the whole round"""
        # print(f"Award brain {self.id} (tag: {self.is_the_tag}) "
        #       f"{self.brain_reward_values['POINTS_FOR_ROUND_WIN']} points for surviving round")
        self.brain.add_points(self.brain_reward_values['POINTS_FOR_ROUND_WIN'],
                              reason=REASON_WIN_ROUND)

    def is_stationary(self, frames) -> bool:
        """frames: has the Car been stationary for this many consecutive frames?"""
        # Obstacle cars claim to be always on the move so they don't get killed
        if self.is_obstacle:
            return False

        if self.speed < 0.05:
            self._frames_stationary += 1
        else:
            self._frames_stationary = 0

        if self._frames_stationary >= frames:
            return True
        else:
            return False

    def update(self, game_area_rect=None, is_subprocess=False, draw_graphics=True):
        # Obstacle car doesn't need to move
        if not self.is_obstacle:
            super().update(game_area_rect=game_area_rect, is_subprocess=is_subprocess, draw_graphics=draw_graphics)


class RaceCar(BaseCar):
    # TODO: Very broken because of all the updates. Fix one day.
    """ Car controls most of the stats of Brains """
    def __init__(self, level, brain, pos, heading: int,
                 active_groups: Tuple[Group, ...],
                 inactive_group: Group,
                 sensors_group: Group,
                 sensor_templates: Collection[NamedTuple],
                 brain_reward_values: dict,
                 fuel: float,
                 max_speed: float,
                 min_speed: float,
                 turning_speed: float,
                 acceleration: float,
                 braking: float,
                 friction: float,
                 ):
        super().__init__(level, brain, pos, heading,
                         active_groups=active_groups,
                         inactive_group=inactive_group,
                         sensors_group=sensors_group,
                         sensor_templates=sensor_templates,
                         brain_reward_values=brain_reward_values,
                         fuel=fuel,
                         max_speed=max_speed,
                         min_speed=min_speed,
                         turning_speed=turning_speed,
                         acceleration=acceleration,
                         braking=braking,
                         friction=friction
                         )

        # Label text - shows brain ID
        self.label_text = Text(pos, str(self.brain.id), font_size=14, group=CAR_NUMBERS_GROUP)
        self.score_text = Text(pos, str(self.brain.points_this_round), font_size=14, group=CAR_NUMBERS_GROUP)

        self.backtrack_markers: Deque[Rect] = deque(maxlen=BACTRACK_MARKERS_SAVED)

        # Used to check if lap was valid. Basic - needs a better way.
        self._distance_traveled_this_lap = 0
        # Used for lap times
        self._frames_taken_this_lap = 0
        self.laps_completed = 0

    def __repr__(self):
        return f"<Car with brain {self.brain}>"

    def update(self, game_area_rect=None, is_subprocess=False, draw_graphics=True):
        sensor_values = self.get_sensor_values()
        sensor_values.extend([0, 0, 0, 0])

        # print(f"sensor values: {sensor_values}")

        accelerate, brake, turn_left, turn_right = self.brain.process_sensor_readings(sensor_values)
        #    RaceBrainInputTuple(*self.get_sensor_values(), 0, 0, 0, 0))
        # print(f"Commands from brain {self.brain.id}: {accelerate}, {brake}, {turn_left}, {turn_right}")
        if accelerate:
            self.accelerate(accelerate)
        if brake:
            self.brake(brake)
        if turn_left:
            self.turn_left(turn_left)
        if turn_right:
            self.turn_right(turn_right)

        # Limit speed - max/min values
        self.speed = min(self.max_speed, self.speed)
        self.speed = max(self.min_speed, self.speed)

        # Stats
        self._distance_traveled_this_lap += self.speed
        self._frames_taken_this_lap += 1
        self.brain.distance_traveled += self.speed
        self.brain.frames_alive += 1
        self.brain.add_points(self.speed * POINTS_PER_DISTANCE_TRAVELED,
                              reason=REASON_DISTANCE)
        self.brain.add_points(1 * POINTS_PER_LIFETIME_TICK,
                              reason=REASON_LIFETIME)

        # Fuel
        self._fuel -= self.speed
        if self._fuel <= 0:
            print(f"Car {self.id} out of fuel")
            self.crash()

        # print(f"Speed: {self.speed}, heading: {self.heading}")
        velocity = pygame.math.Vector2()
        velocity.from_polar((self.speed, 270 - self.heading))
        # print(f"Velocity from polar: {velocity}, {velocity.as_polar()}")
        self.position += velocity

        # Check collision with bactrack markers
        # Done with colliderect for speed - pixel perfection not needed here
        for marker in self.backtrack_markers:
            if marker.colliderect(self.rect):
                # print(f"BACKTRACK PENALTY FOR BRAIN {self.brain.id}")
                self.brain.add_points(BACKTRACK_PENALTY_PER_MARKER,
                                      reason=REASON_BACKTRACK)

        # Leave a new marker every BACKTRACK_MARKER_LEAVE_INTERVAL frames
        if self.brain.frames_alive % BACKTRACK_MARKER_LEAVE_INTERVAL == 0:

            # Leave a new backtrack marker at car's position
            # Done AFTER collision checking - otherwise would collide right away
            self.backtrack_markers.appendleft(Rect(self.position[0] - BACKTRACK_MARKER_SIZE // 2,
                                                   self.position[1] - BACKTRACK_MARKER_SIZE // 2,
                                                   BACKTRACK_MARKER_SIZE,
                                                   BACKTRACK_MARKER_SIZE))

        # Crash if outside game area
        if self.rect.left < game_area_rect.left or self.rect.right > game_area_rect.right or \
                self.rect.top < game_area_rect.top or self.rect.bottom > game_area_rect.bottom:
            # print(f"Brain {self.brain.id} outside game area - crash")
            self.crash()

        if not is_subprocess and DRAW_GRAPHICS:
            self.update_graphics()

        # print(self)

    def update_graphics(self):
        self.rotate_image()

        # Update label text
        # self.label_text.text = f"{self.brain.id}"
        if SHOW_SCORES_ON_CARS:
            self.score_text.text = f"{int(self.brain.points_this_round)}"

        x, y = self.rect.center
        if SHOW_CAR_NUMBERS and SHOW_SCORES_ON_CARS:
            self.label_text.pos = x, y - 5
            self.score_text.pos = x, y + 5
        elif SHOW_CAR_NUMBERS and not SHOW_SCORES_ON_CARS:
            self.label_text.pos = x, y
        elif not SHOW_CAR_NUMBERS and SHOW_SCORES_ON_CARS:
            self.score_text.pos = x, y

        # Fuel gauge
        if SHOW_FUEL_GAUGE:
            fuel_ratio = self._fuel / CAR_FUEL
            gauge_max_width = 30
            gauge_width = fuel_ratio * gauge_max_width
            start_pos = self.rect.width / 2 - gauge_max_width / 2, self.rect.height - 3
            end_pos = start_pos[0] + gauge_width, self.rect.height - 3
            gauge_color = GREEN if fuel_ratio > 0.3 else RED
            pygame.draw.line(self.image, gauge_color, start_pos, end_pos)

    def crash(self):
        # Remove from active group
        self.kill()
        # self.label_text.kill()
        # for sensor in self.sensors:
        #     sensor.kill()

        self.brain.crashes += 1

        self.brain.add_points(POINTS_PER_CRASH,
                              reason=REASON_CRASH)

        # print(f"CRASHED! {self}")

    def kill(self):
        """ Kills label text and sensor as well """
        self.label_text.kill()
        self.score_text.kill()
        for sensor in self.sensors:
            sensor.kill()
        pygame.sprite.Sprite.kill(self)

    def check_lap_finished(self):
        """
        Called when car collides with the finish line
        Check whether the lap was valid
        If it was, add stats and refuel
        Crash if car tries to finish a lap from the wrong direction
        """
        direction_difference = abs(angle_difference(self.heading, self.level.start_direction))
        # print(f"Car and level start angle difference: {direction_difference}")

        if direction_difference > FINISH_LINE_ALLOWED_HEADING_DIFFERENCE:
            # WRONG DIRECTION! CRASH!
            # print(f"Brain {self.brain.id} WRONG DIRECTION ON FINISH LINE! CRASH! (level: {self.level.start_direction} -"
            #       f"self: {self.heading} - diff: {direction_difference})")
            self.crash()

        elif self._distance_traveled_this_lap > self.level.lap_minimum_distance:
            # print(f"{self} finished a lap!")
            self.level.add_fastest_lap_if_applicable(self._frames_taken_this_lap, self.brain)
            self.brain.laps_completed += 1
            self.laps_completed += 1
            # rounds_started handled by main - knows number of laps
            points_before_lap_bonus_and_deduction = self.brain.points_this_round
            # print("---------------")
            # print(f"Brain {self.brain.id} points before lap bonus: {self.brain.points_this_round}")
            self.brain.add_points(POINTS_PER_LAP, reason=REASON_LAP)
            # print(f"Brain {self.brain.id} points before distance deduction: {self.brain.points_this_round}")
            # Remove points for distance traveled - encourage efficient route
            self.brain.add_points(POINTS_PER_DISTANCE_TRAVELED * self._distance_traveled_this_lap * -1,
                                  reason=REASON_DISTANCE)
            # print(f"Brain {self.brain.id} points after deduction: {self.brain.points_this_round}")
            points_after_lap_bonus_and_deduction = self.brain.points_this_round
            assert points_after_lap_bonus_and_deduction > points_before_lap_bonus_and_deduction, \
                f"ERROR: Brain points after finishing lap LOWER than before finishing!\n" \
                f"Before: {points_after_lap_bonus_and_deduction} After: {points_after_lap_bonus_and_deduction}"
            self._distance_traveled_this_lap = 0
            self._frames_taken_this_lap = 0
            self._fuel = CAR_FUEL
            # Clear backtrack markers
            self.backtrack_markers.clear()
            # print(f"Points after lap finish: {self.brain.points}")


if __name__ == '__main__':
    pygame.init()
    pygame.display.set_mode((1, 1))
    car = FakeCar()
