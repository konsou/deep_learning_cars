from typing import Sequence, Tuple


def intify_tuple(tpl: Sequence[float]) -> Tuple[int, int]:
    return int(tpl[0]), int(tpl[1])


class DotDict(dict):
    """Dictionary that can be accessed by dot notation.
    Normal dict: my_dict['key']
    DotDict: my_dict.key"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


if __name__ == '__main__':
    my_dict = DotDict({"jou": "jee", "terve": "moi"})
    print(my_dict)
    print(my_dict.jou)
