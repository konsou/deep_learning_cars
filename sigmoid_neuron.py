import math
import random
import pickle
from evolution import variance
from constants import *


def logistic(x):
    return 1 / (1 + math.e**(x * -1))


class SigmoidNeuron:
    id_counter = 0

    def __init__(self, number_of_inputs):
        self.id = SigmoidNeuron.id_counter
        SigmoidNeuron.id_counter += 1

        self.number_of_inputs = number_of_inputs
        self.input_values = [None for _ in range(number_of_inputs)]
        self.weights = [random.uniform(-1, 1) for _ in range(number_of_inputs)]
        self.bias = random.uniform(-1, 1)

        # print(f"Init {self}")

    def __repr__(self):
        return f"<SigmoidNeuron {self.id} - {self.number_of_inputs} inputs>"

    def update_inputs(self, input_values: iter):
        self.input_values = input_values

    def get_value(self):
        input_sum = 0
        for index, input_ in enumerate(self.input_values):
            input_sum += input_ * self.weights[index]

        return logistic(input_sum + self.bias)

    def evolve(self) -> None:
        # print(f"{self} - bias {self.bias} - weights {self.weights} - evolving")

        self.bias += variance()
        self.weights = [i + variance() for i in self.weights]
        # print(f"{self} - bias {self.bias} - weights {self.weights}")


class InputNeuron:
    """ DEPRECATED """
    def __init__(self, value=0):
        self._value = value
        print(f"Init {self}")

    def __repr__(self):
        return f"<InputNeuron - value {self.get_value()}>"

    def get_value(self):
        return self._value

    def set_value(self, value):
        self._value = value


if __name__ == '__main__':
    input_layer = []
    first_layer = []
    output_layer = []

    for i in range(4):
        input_layer.append(InputNeuron(random.getrandbits(1)))

    for i in range(3):
        first_layer.append(SigmoidNeuron(input_layer))

    for i in range(4):
        output_layer.append(SigmoidNeuron(first_layer))

    print(first_layer[2].get_value())
